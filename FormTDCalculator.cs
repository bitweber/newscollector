﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlashNDCollection
{
    public partial class FormTDCalculator : Form
    {
        public FormTDCalculator()
        {
            InitializeComponent();
        }
        private struct Metal
        {
            public double OpenPrice;
            public double LastClosePrice;
            public double Updown;
        }

        private void FormCalculator_Load(object sender, EventArgs e)
        {
        }

        //限制输入
        private void onlyEnterNumber(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 13 && e.KeyChar != 45 && e.KeyChar != 46)
            {
                e.Handled = true;
            }

            //输入为负号时，只能输入一次且只能输入一次
            if (e.KeyChar == 45 && (((TextBox)sender).SelectionStart != 0 || ((TextBox)sender).Text.IndexOf("-") >= 0)) e.Handled = true;
            if (e.KeyChar == 46 && ((TextBox)sender).Text.IndexOf(".") >= 0) e.Handled = true;
        }

        private void lblGDBtn_Click(object sender, EventArgs e)
        {

            string goldBaojia = BaojiaGold();
            if (goldBaojia != null && goldBaojia != "")
            {
                Clipboard.SetText(goldBaojia);
            }

        }

        private void lblSliverBtn_Click(object sender, EventArgs e)
        {
            string sliverBaojia = BaojiaSliver();

            if (sliverBaojia != null && sliverBaojia != "")
            {
                Clipboard.SetText(sliverBaojia);
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            string baojia = BaojiaGold() + BaojiaSliver();

            if (baojia != null && baojia != "")
            {
                Clipboard.SetText(baojia);
            }
        }
        private string BaojiaGold()
        {
            Metal gold;
            string strOut;
            if (txtGDOpen.Text != null && txtGDOpen.Text != "")
            {
                gold.OpenPrice = double.Parse(txtGDOpen.Text);
            }
            else
            {
                gold.OpenPrice = 0;
            }

            if (txtGDLast.Text != null && txtGDLast.Text != "")
            {
                gold.LastClosePrice = double.Parse(txtGDLast.Text);
            }
            else
            {
                gold.LastClosePrice = 0;
            }

            gold.Updown = gold.OpenPrice - gold.LastClosePrice;
            lblGDUD.Text = string.Format("{0:F}", gold.Updown);

            if (gold.Updown < 0)
            {
                lblGDUD.ForeColor = Color.Blue;
                strOut = string.Format("黄金TD开盘报{0}元/克，下跌{1}元；", txtGDOpen.Text, lblGDUD.Text.Replace("-", ""));
            }
            else if (gold.Updown == 0)
            {
                lblGDUD.ForeColor = Color.Black;
                strOut = string.Format("黄金TD开盘报{0}元/克，与昨日收盘持平；", txtGDOpen.Text);
            }
            else
            {
                lblGDUD.ForeColor = Color.Red;
                strOut = string.Format("黄金TD开盘报{0}元/克，上涨{1}元；", txtGDOpen.Text, lblGDUD.Text);
            }

            return strOut;
        }
        private string BaojiaSliver()
        {
            Metal sliver;
            string strOut;
            if (txtSliverOpen.Text != null && txtSliverOpen.Text != "")
            {
                sliver.OpenPrice = double.Parse(txtSliverOpen.Text);
            }
            else
            {
                sliver.OpenPrice = 0;
            }

            if (txtSliverLast.Text != null && txtSliverLast.Text != "")
            {
                sliver.LastClosePrice = double.Parse(txtSliverLast.Text);
            }
            else
            {
                sliver.LastClosePrice = 0;
            }

            sliver.Updown = sliver.OpenPrice - sliver.LastClosePrice;
            lblSliverUD.Text = string.Format("{0}", (int)sliver.Updown);

            if (sliver.Updown < 0)
            {
                lblSliverUD.ForeColor = Color.Blue;
                strOut = string.Format("白银TD开盘报{0}元/千克，下跌{1}元。", txtSliverOpen.Text, lblSliverUD.Text).Replace("-", "");
            }
            else if (sliver.Updown == 0)
            {
                lblSliverUD.ForeColor = Color.Black;
                strOut = string.Format("白银TD开盘报{0}元/千克，与昨日收盘持平。", txtSliverOpen.Text);
            }
            else
            {
                lblSliverUD.ForeColor = Color.Red;
                strOut = string.Format("白银TD开盘报{0}元/千克，上涨{1}元。", txtSliverOpen.Text, lblSliverUD.Text);
            }

            return strOut;
        }
    }
}
