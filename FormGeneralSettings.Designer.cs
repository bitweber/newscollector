﻿namespace FlashNDCollection
{
    partial class FormGeneralSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGeneralSettings));
            this.cbxCatchClipboard = new System.Windows.Forms.CheckBox();
            this.grbBaseFunction = new System.Windows.Forms.GroupBox();
            this.grbHighFunction = new System.Windows.Forms.GroupBox();
            this.txtRemovingStr = new System.Windows.Forms.TextBox();
            this.cbxRemoveTxt = new System.Windows.Forms.CheckBox();
            this.btnSure = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.grbBaseFunction.SuspendLayout();
            this.grbHighFunction.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxCatchClipboard
            // 
            this.cbxCatchClipboard.AutoSize = true;
            this.cbxCatchClipboard.Location = new System.Drawing.Point(19, 20);
            this.cbxCatchClipboard.Name = "cbxCatchClipboard";
            this.cbxCatchClipboard.Size = new System.Drawing.Size(135, 21);
            this.cbxCatchClipboard.TabIndex = 5;
            this.cbxCatchClipboard.Text = "开启监视剪切板功能";
            this.cbxCatchClipboard.UseVisualStyleBackColor = true;
            this.cbxCatchClipboard.CheckedChanged += new System.EventHandler(this.cbxCatchClipboard_CheckedChanged);
            // 
            // grbBaseFunction
            // 
            this.grbBaseFunction.Controls.Add(this.checkBox4);
            this.grbBaseFunction.Controls.Add(this.checkBox3);
            this.grbBaseFunction.Controls.Add(this.checkBox2);
            this.grbBaseFunction.Controls.Add(this.checkBox1);
            this.grbBaseFunction.Location = new System.Drawing.Point(4, 6);
            this.grbBaseFunction.Name = "grbBaseFunction";
            this.grbBaseFunction.Size = new System.Drawing.Size(349, 91);
            this.grbBaseFunction.TabIndex = 3;
            this.grbBaseFunction.TabStop = false;
            this.grbBaseFunction.Text = "数据显示格式";
            // 
            // grbHighFunction
            // 
            this.grbHighFunction.Controls.Add(this.txtRemovingStr);
            this.grbHighFunction.Controls.Add(this.cbxRemoveTxt);
            this.grbHighFunction.Controls.Add(this.cbxCatchClipboard);
            this.grbHighFunction.Location = new System.Drawing.Point(4, 103);
            this.grbHighFunction.Name = "grbHighFunction";
            this.grbHighFunction.Size = new System.Drawing.Size(349, 128);
            this.grbHighFunction.TabIndex = 4;
            this.grbHighFunction.TabStop = false;
            this.grbHighFunction.Text = "监视器设置";
            // 
            // txtRemovingStr
            // 
            this.txtRemovingStr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRemovingStr.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtRemovingStr.Location = new System.Drawing.Point(19, 67);
            this.txtRemovingStr.Multiline = true;
            this.txtRemovingStr.Name = "txtRemovingStr";
            this.txtRemovingStr.Size = new System.Drawing.Size(315, 55);
            this.txtRemovingStr.TabIndex = 7;
            this.txtRemovingStr.TextChanged += new System.EventHandler(this.txtRemovingStr_TextChanged);
            // 
            // cbxRemoveTxt
            // 
            this.cbxRemoveTxt.AutoSize = true;
            this.cbxRemoveTxt.Location = new System.Drawing.Point(19, 43);
            this.cbxRemoveTxt.Name = "cbxRemoveTxt";
            this.cbxRemoveTxt.Size = new System.Drawing.Size(315, 21);
            this.cbxRemoveTxt.TabIndex = 6;
            this.cbxRemoveTxt.Text = "剪切板文本去关键字（请在文本框输入，以逗号隔开）";
            this.cbxRemoveTxt.UseVisualStyleBackColor = true;
            this.cbxRemoveTxt.CheckedChanged += new System.EventHandler(this.cbxRemoveTxt_CheckedChanged);
            // 
            // btnSure
            // 
            this.btnSure.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSure.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSure.Location = new System.Drawing.Point(47, 234);
            this.btnSure.Name = "btnSure";
            this.btnSure.Size = new System.Drawing.Size(111, 31);
            this.btnSure.TabIndex = 8;
            this.btnSure.Text = "确定";
            this.btnSure.UseVisualStyleBackColor = true;
            this.btnSure.Click += new System.EventHandler(this.btnSure_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnExit.Location = new System.Drawing.Point(202, 234);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(111, 31);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "取消";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(19, 22);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(51, 21);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "前值";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(114, 22);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(63, 21);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "预期值";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(19, 64);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(51, 21);
            this.checkBox3.TabIndex = 7;
            this.checkBox3.Text = "影响";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(114, 64);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(75, 21);
            this.checkBox4.TabIndex = 8;
            this.checkBox4.Text = "预期比较";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // FormGeneralSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 271);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSure);
            this.Controls.Add(this.grbHighFunction);
            this.Controls.Add(this.grbBaseFunction);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormGeneralSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "选项";
            this.Load += new System.EventHandler(this.FormSettings_Load);
            this.grbBaseFunction.ResumeLayout(false);
            this.grbBaseFunction.PerformLayout();
            this.grbHighFunction.ResumeLayout(false);
            this.grbHighFunction.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.CheckBox cbxCatchClipboard;
        private System.Windows.Forms.GroupBox grbBaseFunction;
        private System.Windows.Forms.GroupBox grbHighFunction;
        private System.Windows.Forms.TextBox txtRemovingStr;
        private System.Windows.Forms.CheckBox cbxRemoveTxt;
        private System.Windows.Forms.Button btnSure;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}