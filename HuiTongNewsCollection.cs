﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace FlashNDCollection
{
    class HuiTongNewsCollection : ICatchData, ICatchFlashNews
    {
        public HuiTongNewsCollection()
        {
        }

        private string HuiTongUrl = @"http://live.gold678.com/Handler/LiveHandler.ashx?ViewType=Live&Page=1&KeyWords=&Type=1";
        private string HuiTongSiteName = "汇通快讯";

        //#region 新版汇通
        //public List<DataObject> getDataList()
        //{
        //    List<DataObject> datalist = new List<DataObject>();
        //    int dataid = 1;

        //    WebHtmlData webdata = new WebHtmlData(HuiTongUrl);
        //    HtmlDocument doc = new HtmlDocument();
        //    doc.LoadHtml(webdata.HtmlData);

        //    HtmlNode NodeList = doc.GetElementbyId("nowul");

        //    string nodetext = NodeList.InnerHtml;
        //    string shortdateStr = DateTime.Now.ToString("yyyy-MM-dd");//设置当前时间

        //    HtmlNodeCollection NewsDataNodes = NodeList.SelectNodes(@"./li");
        //    HtmlNode timeLineNode = NodeList.SelectSingleNode(@"/div[@class='time_line']/div[@class='time_i']");

        //    if (timeLineNode!=null)
        //    {
        //        string dateFinder = FilterTools.trimAll(FilterTools.regexData(timeLineNode.InnerText,@".*\d"));
        //        //dateFinder = dateFinder.Replace("年", "-").Replace("月", "-").Replace("日", "");
        //        shortdateStr = dateFinder;
        //    }

        //    for (int i = 0; i < NewsDataNodes.Count; i++)
        //    {
        //        int datalevel = 1;
        //        HtmlNode preDataNode = null;
        //        HtmlNode expDataNode = null;
        //        HtmlNode affectDataLiduoNode = null;
        //        HtmlNode affectDataLikongNode = null;
        //        HtmlNode timeNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='inter_content_li']/div[@class='fb_time']");
        //        HtmlNode dataNameNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='inter_content_li']/div[@class='list_font']");
        //        if (dataNameNode!=null)
        //        {
        //            preDataNode = dataNameNode.SelectSingleNode(@"./span[1]/span");
        //            expDataNode = dataNameNode.SelectSingleNode(@"./span[2]/span");
        //        }


        //        HtmlNode actualDataNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='inter_content_li']/div[@class='now_star']");

        //        HtmlNode affectsDataAll = NewsDataNodes[i].SelectSingleNode(@"./div[@class='inter_content_li']/div[@class='getmore2']");
        //        if (affectsDataAll!=null)
        //        {
        //            affectDataLiduoNode = affectsDataAll.SelectSingleNode(@"./ul[@class='red_get']");
        //            affectDataLikongNode = affectsDataAll.SelectSingleNode(@"./ul[@class='green_get']");
        //        }

        //        if (dataNameNode != null && preDataNode != null && actualDataNode != null && timeNode != null)
        //        {

        //            DataObject data = new DataObject();

        //            data.Time = DateTime.ParseExact(string.Format("{0} {1}", shortdateStr, FilterTools.trimAll(timeNode.InnerText)), "yyyy-MM-dd HH:mm:ss", null);
        //            //级别设置
        //            if (NewsDataNodes[i].Attributes["level"] != null)
        //            {
        //                int.TryParse(NewsDataNodes[i].Attributes["level"].Value, out datalevel);
        //            }
        //            data.Level = datalevel;
        //            data.ID = dataid;
        //            data.IsUsed = false;
        //            data.PercentTypeOrNot = false;
        //            data.DataName = FilterTools.trimAll(dataNameNode.InnerText.Remove(dataNameNode.InnerText.IndexOf("前值")));
        //            string predata = FilterTools.trimAll(preDataNode.InnerText);
        //            string actualdata = FilterTools.catchStr(FilterTools.trimAll(actualDataNode.InnerText), "实际值：&nbsp;", "</span>");
        //            data.PreValue = predata;
        //            data.ActualValue = actualdata;
        //            if (predata.Contains("%") == true)
        //            {
        //                data.PercentTypeOrNot = true;
        //            }
        //            if (expDataNode != null)
        //            {
        //                string expdata = FilterTools.trimAll(expDataNode.InnerText);
        //                double tmpexpValue = 0;
        //                if (expdata != "" && double.TryParse(FilterTools.regexData(expdata.Replace("%",""), @".*\d"),out tmpexpValue)==true)
        //                {
        //                    data.Expectedvalue = expdata;
        //                }
        //                else
        //                {
        //                    data.Expectedvalue = "";
        //                }
        //            }

        //            if (data.Expectedvalue != null && data.Expectedvalue != "") //如果存在预期值
        //            {
        //                //预期比较
        //                if (data.PercentTypeOrNot == true)
        //                {
        //                    double dataAc = double.Parse(data.ActualValue.Replace("%", ""));
        //                    double dataExp = double.Parse(data.Expectedvalue.Replace("%", ""));
        //                    if (dataAc > dataExp)
        //                    {
        //                        if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
        //                        {
        //                            data.AcPreCompare = "不及预期";
        //                        }
        //                        else
        //                        {
        //                            data.AcPreCompare = "好于预期";
        //                        }
        //                    }
        //                    else if (dataAc == dataExp)
        //                    {
        //                        data.AcPreCompare = "符合预期";
        //                    }
        //                    else
        //                    {
        //                        if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
        //                        {
        //                            data.AcPreCompare = "好于预期";
        //                        }
        //                        else
        //                        {
        //                            data.AcPreCompare = "不及预期";
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    double dataAc = double.Parse(FilterTools.regexData(data.ActualValue, @".*\d"));
        //                    double dataExp = double.Parse(FilterTools.regexData(data.Expectedvalue, @".*\d"));
        //                    if (dataAc > dataExp)
        //                    {
        //                        if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
        //                        {
        //                            data.AcPreCompare = "不及预期";
        //                        }
        //                        else
        //                        {
        //                            data.AcPreCompare = "好于预期";
        //                        }
        //                    }
        //                    else if (dataAc == dataExp)
        //                    {
        //                        data.AcPreCompare = "符合预期";
        //                    }
        //                    else
        //                    {
        //                        if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
        //                        {
        //                            data.AcPreCompare = "好于预期";
        //                        }
        //                        else
        //                        {
        //                            data.AcPreCompare = "不及预期";
        //                        }
        //                    }
        //                }
        //            }

        //            //多空选择
        //            data.DataAffacts = "影响较小";
        //            if (affectsDataAll != null)
        //            {
        //                if (affectDataLiduoNode != null)
        //                {
        //                    string liduo = FilterTools.trimAll(affectDataLiduoNode.InnerText);

        //                    if (liduo.Contains("金银"))
        //                    {
        //                        data.DataAffacts = "黄金或将受支撑";
        //                    }                           
        //                }

        //                if (affectDataLikongNode != null)
        //                {
        //                    string likong = FilterTools.trimAll(affectDataLikongNode.InnerText);
        //                    if (likong.Contains("金银"))
        //                    {
        //                        data.DataAffacts = "黄金或将承压";
        //                    }                            
        //                }
        //            }

        //            data.FromSite = HuiTongSiteName;
        //            //过滤数据
        //            if (FilterTools.Level != 0)
        //            {
        //                if (data.Level == FilterTools.Level)
        //                {
        //                    datalist.Add(data);
        //                    dataid++;
        //                }
        //            }
        //            else
        //            {
        //                datalist.Add(data);
        //                dataid++;
        //            }
        //        }
        //    }
        //    return datalist;
        //}

        //public List<FlashNewsObject> getFlashNewsList()
        //{
        //    List<FlashNewsObject> newslist = new List<FlashNewsObject>();
        //    int newsid = 1;

        //    WebHtmlData webdata = new WebHtmlData(HuiTongUrl);
        //    HtmlDocument doc = new HtmlDocument();
        //    doc.LoadHtml(webdata.HtmlData);

        //    HtmlNode NodeList = doc.GetElementbyId("nowul");

        //    string nodetext = NodeList.InnerHtml;
        //    string shortdateStr = DateTime.Now.ToString("yyyy-MM-dd");//设置当前时间

        //    HtmlNodeCollection NewsDataNodes = NodeList.SelectNodes(@"./li");

        //    HtmlNode timeLineNode = NodeList.SelectSingleNode(@"./div[@class='time_line']/div[@class='time_i']");

        //    if (timeLineNode!=null)
        //    {
        //        string dateFinder = FilterTools.trimAll(FilterTools.regexData(timeLineNode.InnerText, @".*\d"));
        //        //dateFinder = dateFinder.Replace("年", "-").Replace("月", "-").Replace("日", "");
        //        shortdateStr = dateFinder;
        //    }

        //    for (int i = 0; i < NewsDataNodes.Count; i++)
        //    {

        //        int newslevel = 1;

        //        HtmlNode timeNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='inter_content_li']/div[@class='fb_time']");
        //        HtmlNode newsNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='inter_content_li']/div[@class='list_font_p']");

        //        if (newsNode != null && timeNode != null)
        //        {
        //            FlashNewsObject news = new FlashNewsObject();
        //            news.ID = newsid;
        //            news.IsUsed = false;
        //            //级别设置
        //            if (NewsDataNodes[i].Attributes["level"] != null)
        //            {
        //                int.TryParse(NewsDataNodes[i].Attributes["level"].Value, out newslevel);
        //            }
        //            news.Level = newslevel;
        //            news.Time = DateTime.ParseExact(string.Format("{0} {1}", shortdateStr, FilterTools.trimAll(timeNode.InnerText)), "yyyy-MM-dd HH:mm:ss", null);
        //            //新闻过滤缩减
        //            HtmlNodeCollection newsParagraphs = newsNode.SelectNodes(@"./a");
        //            if (newsParagraphs!= null && newsParagraphs.Count > 1)
        //            {
        //                string spaceTag = "&nbsp";
        //                //string tailTime = newsParagraphs[0].SelectSingleNode(@"./i").InnerText;
        //                //string newstitle = newsParagraphs[0].InnerText.Replace(tailTime, "").Replace(spaceTag, "");
        //                string tmpnews = "";
        //                foreach (var item in newsParagraphs)
        //                {
        //                    string tailTime = item.SelectSingleNode(@"./i").InnerText;
        //                    string newsItem = item.InnerText.Replace(tailTime, "").Replace(spaceTag, "");
        //                    tmpnews = tmpnews + FilterTools.trimAll(newsItem) + "\r\n";             
        //                }
        //                news.News = tmpnews.TrimEnd('\n').TrimEnd('\r').TrimEnd(';') + "。";
        //            }
        //            else
        //            {
        //                news.News = newsNode.InnerText.Replace("\t", "").Trim().TrimEnd(';') + "。";
        //            }

        //            news.FromSite = HuiTongSiteName;
        //            //过滤新闻
        //            if (FilterTools.Level != 0)
        //            {
        //                if (news.Level == FilterTools.Level)
        //                {
        //                    newslist.Add(news);
        //                    newsid++;
        //                }
        //            }
        //            else
        //            {
        //                newslist.Add(news);
        //                newsid++;
        //            }
        //        }
        //    }
        //    return newslist;
        //}
        //#endregion

        #region 旧版汇通
        public List<DataObject> getDataList()
        {
            List<DataObject> datalist = new List<DataObject>();
            int dataid = 1;

            WebHtmlData webdata = new WebHtmlData(HuiTongUrl);
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(webdata.HtmlData);

            HtmlNode NodeList = doc.GetElementbyId("nowul");

            string nodetext = NodeList.InnerHtml;
            string shortdateStr = DateTime.Now.ToString("yyyy-MM-dd");//设置当前时间

            HtmlNodeCollection NewsDataNodes = NodeList.SelectNodes(@"./tr[@class='body_zb_li']");

            for (int i = 0; i < NewsDataNodes.Count; i++)
            {
                int datalevel = 1;
                //if (NewsDataNodes[i].Attributes["class"] != null && NewsDataNodes[i].Attributes["class"].Value == "day_line")
                //{
                //    string dateFinder = FilterTools.trimAll(NewsDataNodes[i].InnerText);
                //    dateFinder = dateFinder.Replace("年", "-").Replace("月", "-").Replace("日", "");
                //    shortdateStr = dateFinder;
                //}

                if (NewsDataNodes[i].SelectSingleNode(@"./td[1]").Attributes["class"] != null && NewsDataNodes[i].SelectSingleNode(@"./td[1]").Attributes["class"].Value == "data_time")
                {
                    string dateFinder = FilterTools.trimAll(NewsDataNodes[i].SelectSingleNode(@"./td[1]").InnerText);
                    //dateFinder = dateFinder.Replace("年", "-").Replace("月", "-").Replace("日", "");
                    dateFinder = FilterTools.regexData(dateFinder, @".*/d");
                    shortdateStr = dateFinder;
                }

                HtmlNode timeNode = NewsDataNodes[i].SelectSingleNode(@"./td[@class='zb_time']"); //更新时间

                HtmlNode dataNameNode = NewsDataNodes[i].SelectSingleNode(@"./td[3]");//数据名称

                if (dataNameNode != null)
                {
                    Console.WriteLine("1");
                }
                HtmlNode preDataNode = NewsDataNodes[i].SelectSingleNode(@"./td[3]/span[1]/span");//前值
                HtmlNode expDataNode = NewsDataNodes[i].SelectSingleNode(@"./td[3]/span[2]/span");//预期值

                HtmlNode actualDataNode = NewsDataNodes[i].SelectSingleNode(@"./td[4]/span");//实际值

                HtmlNode affectsDataAll = NewsDataNodes[i].SelectSingleNode(@"./td[5]/div");//影响
                HtmlNode affectDataLiduoNode = NewsDataNodes[i].SelectSingleNode(@"./td[5]/ul[@class='red_get']");//利多影响
                HtmlNode affectDataLikongNode = NewsDataNodes[i].SelectSingleNode(@"./td[5]/ul[@class='green_get']");//利空影响

                if (dataNameNode != null && preDataNode != null && actualDataNode != null && timeNode != null)
                {

                    DataObject data = new DataObject();

                    data.Time = DateTime.ParseExact(string.Format("{0} {1}", shortdateStr, FilterTools.trimAll(timeNode.InnerText)), "yyyy-MM-dd HH:mm:ss", null);
                    //级别设置
                    if (NewsDataNodes[i].SelectSingleNode(@"./td[4]/div").Attributes["class"] != null)
                    {
                        int.TryParse(NewsDataNodes[i].SelectSingleNode(@"./td[4]/div").Attributes["class"].Value.Replace("star_xz star_", ""), out datalevel);
                    }
                    data.Level = datalevel;
                    data.ID = dataid;
                    data.IsUsed = false;
                    data.PercentTypeOrNot = false;
                    data.DataName = FilterTools.trimAll(dataNameNode.InnerText.Remove(dataNameNode.InnerText.IndexOf("前值")));
                    string predata = FilterTools.trimAll(preDataNode.InnerText);
                    string actualdata = FilterTools.trimAll(actualDataNode.InnerText);
                    data.PreValue = predata;
                    data.ActualValue = actualdata;
                    if (predata.Contains("%") == true)
                    {
                        data.PercentTypeOrNot = true;
                    }
                    if (expDataNode != null)
                    {
                        string expdata = FilterTools.trimAll(expDataNode.InnerText);
                        double tmpexpValue = 0;
                        if (expdata != "" && double.TryParse(FilterTools.regexData(expdata.Replace("%", ""), @".*\d"), out tmpexpValue) == true)
                        {
                            data.Expectedvalue = expdata;
                        }
                        else
                        {
                            data.Expectedvalue = "";
                        }
                    }

                    if (data.Expectedvalue != null && data.Expectedvalue != "") //如果存在预期值
                    {
                        //预期比较
                        if (data.PercentTypeOrNot == true)
                        {
                            double dataAc = double.Parse(data.ActualValue.Replace("%", ""));
                            double dataExp = double.Parse(data.Expectedvalue.Replace("%", ""));
                            if (dataAc > dataExp)
                            {
                                //if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
                                //{
                                //    data.AcPreCompare = "不及预期";
                                //}
                                //else
                                //{
                                //    data.AcPreCompare = "好于预期";
                                //}
                                data.AcPreCompare = "好于预期";
                            }
                            else if (dataAc == dataExp)
                            {
                                data.AcPreCompare = "符合预期";
                            }
                            else
                            {
                                //if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
                                //{
                                //    data.AcPreCompare = "好于预期";
                                //}
                                //else
                                //{
                                //    data.AcPreCompare = "不及预期";
                                //}
                                data.AcPreCompare = "不及预期";
                            }
                        }
                        else
                        {
                            double dataAc = double.Parse(FilterTools.regexData(data.ActualValue, @".*\d"));
                            double dataExp = double.Parse(FilterTools.regexData(data.Expectedvalue, @".*\d"));
                            if (dataAc > dataExp)
                            {
                                //if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
                                //{
                                //    data.AcPreCompare = "不及预期";
                                //}
                                //else
                                //{
                                //    data.AcPreCompare = "好于预期";
                                //}
                                data.AcPreCompare = "好于预期";
                            }
                            else if (dataAc == dataExp)
                            {
                                data.AcPreCompare = "符合预期";
                            }
                            else
                            {
                                //if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
                                //{
                                //    data.AcPreCompare = "好于预期";
                                //}
                                //else
                                //{
                                //    data.AcPreCompare = "不及预期";
                                //}
                                data.AcPreCompare = "不及预期";
                            }
                        }
                    }

                    //多空选择
                    data.DataAffacts = "影响较小";
                    if (affectsDataAll != null)
                    {
                        if (affectDataLiduoNode != null)
                        {
                            string liduo = FilterTools.trimAll(affectDataLiduoNode.InnerText);

                            if (liduo.Contains("金银"))
                            {
                                data.DataAffacts = "黄金或将受支撑";
                            }
                        }

                        if (affectDataLikongNode != null)
                        {
                            string likong = FilterTools.trimAll(affectDataLikongNode.InnerText);
                            if (likong.Contains("金银"))
                            {
                                data.DataAffacts = "黄金或将承压";
                            }
                        }
                    }

                    data.FromSite = HuiTongSiteName;
                    //过滤数据
                    if (FilterTools.Level != 0)
                    {
                        if (data.Level == FilterTools.Level)
                        {
                            datalist.Add(data);
                            dataid++;
                        }
                    }
                    else
                    {
                        datalist.Add(data);
                        dataid++;
                    }
                }
            }
            return datalist;
        }

        //public List<FlashNewsObject> getFlashNewsList()
        //{
        //    List<FlashNewsObject> newslist = new List<FlashNewsObject>();
        //    int newsid = 1;

        //    WebHtmlData webdata = new WebHtmlData(HuiTongUrl);
        //    HtmlDocument doc = new HtmlDocument();
        //    doc.LoadHtml(webdata.HtmlData);

        //    HtmlNode NodeList = doc.GetElementbyId("nowul");

        //    string nodetext = NodeList.InnerHtml;
        //    string shortdateStr = DateTime.Now.ToString("yyyy-MM-dd");//设置当前时间

        //    HtmlNodeCollection NewsDataNodes = NodeList.SelectNodes(@"./ul/li");

        //    for (int i = 0; i < NewsDataNodes.Count; i++)
        //    {

        //        int newslevel = 1;

        //        HtmlNode datatimeNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='data_time']/div[@class='time_i']");

        //        if (datatimeNode != null && datatimeNode.InnerText != null & datatimeNode.InnerText != "")
        //        {
        //            string dateFinder = FilterTools.trimAll(datatimeNode.InnerText);
        //            dateFinder = dateFinder.Replace("年", "-").Replace("月", "-").Replace("日", "");
        //            dateFinder = FilterTools.regexData(dateFinder, @"(?<=).*(?=星)");
        //            if (dateFinder != "")
        //            {
        //                shortdateStr = dateFinder;
        //            }
        //        }

        //        HtmlNode timeNode = NewsDataNodes[i].SelectSingleNode(@"./div[1][@class='zb_time']/a");
        //        HtmlNode newsNode = NewsDataNodes[i].SelectSingleNode(@"./div[2][@class='zb_word']/div[1]/a[1]");

        //        if (newsNode != null && timeNode != null)
        //        {
        //            FlashNewsObject news = new FlashNewsObject();
        //            news.ID = newsid;
        //            news.IsUsed = false;
        //            //级别设置
        //            //if (NewsDataNodes[i].Attributes["level"] != null)
        //            //{
        //            //    int.TryParse(NewsDataNodes[i].Attributes["level"].Value, out newslevel);
        //            //}

        //            if (newsNode.Attributes["class"] != null && newsNode.Attributes["class"].Value.Contains("red_color_f"))
        //            {
        //                newslevel = 3;
        //            }

        //            news.Level = newslevel;

        //            news.Time = DateTime.ParseExact(string.Format("{0} {1}", shortdateStr, FilterTools.trimAll(timeNode.InnerText)), "yyyy-MM-dd HH:mm:ss", null);

        //            string tmpnews = newsNode.InnerText.Replace("\t", "").Replace("&nbsp","").Replace(" ","").Trim();

        //            string[] tmpnewsArr = tmpnews.Split('\n');
        //            if (tmpnewsArr.Length > 1)
        //            {
        //                tmpnews = "";
        //                foreach (var item in tmpnewsArr)
        //                {
        //                    if (item.Trim()!=null && item.Trim() != "")
        //                    {
        //                        tmpnews = tmpnews + "\r\n" + item.Trim();
        //                    }
        //                }
        //            }

        //            news.News = tmpnews.TrimEnd('\n').TrimEnd('\r').TrimEnd('！').TrimEnd('？').TrimEnd('；').TrimEnd('。').Trim() + "。";
        //            news.FromSite = HuiTongSiteName;
        //            //过滤新闻
        //            if (FilterTools.Level != 0)
        //            {
        //                if (news.Level == FilterTools.Level)
        //                {
        //                    newslist.Add(news);
        //                    newsid++;
        //                }
        //            }
        //            else
        //            {
        //                newslist.Add(news);
        //                newsid++;
        //            }
        //        }
        //    }
        //    return newslist;
        //}

        #endregion

        #region fast huijin
        public List<FlashNewsObject> getFlashNewsList()
        {
            List<FlashNewsObject> newslist = new List<FlashNewsObject>();
            int newsid = 1;

            WebHtmlData webdata = new WebHtmlData(HuiTongUrl);

            FlashNewsObject news = new FlashNewsObject();
            string newsjson = webdata.HtmlData;

            JArray jarr = (JArray)JsonConvert.DeserializeObject(newsjson);

            for (int i = 0; i < jarr.Count; i++)
            {
                JArray jarr2 = (JArray)JsonConvert.DeserializeObject(jarr[i].ToString());

                for (int j = 0; j < jarr2.Count; j++)
                {
                    Dictionary<string, object> newsdic = JsonConvert.DeserializeObject<Dictionary<string, object>>(jarr2[j].ToString());
                    if (newsdic["NewsType"].ToString() == "0")
                    {
                        news = new FlashNewsObject();
                        news.ID = newsid;
                        news.Time = FilterTools.StampToDateTime(newsdic["NewsTime"].ToString());

                        news.News = newsdic["NewsTitle"].ToString()+"。";

                        news.Level = Int32.Parse(newsdic["NewsLevel"].ToString());

                        news.FromSite = HuiTongSiteName;
                        newslist.Add(news);
                        newsid++;
                    }

                }
            }
            return newslist;
        }

        #endregion
    }
}
