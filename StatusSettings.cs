﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashNDCollection
{
    public static class StatusSettings
    {
        //当前页码
        private static int pageNum=0;

        public static int PageNum
        {
            get { return pageNum; }
            set { pageNum = value; }
        }

        private static int endPageNum = 0;

        public static int EndPageNum
        {
            get { return endPageNum; }
            set { endPageNum = value; }
        }
        //当前选中数据
        private static string selectDatas;

        public static string SelectDatas
        {
            get { return selectDatas; }
            set { selectDatas = value; }
        }



    }
}
