﻿namespace FlashNDCollection
{
    partial class MainFormNewsCollection
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFormNewsCollection));
            this.stspState = new System.Windows.Forms.StatusStrip();
            this.tstlblTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tstlblNow = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslblTmppage = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsspage = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssSpace = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLastUpdated = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssupdatedTimestr = new System.Windows.Forms.ToolStripStatusLabel();
            this.tspFunctions = new System.Windows.Forms.ToolStrip();
            this.tsbtnInspect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnCondtions = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnShowSet = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tscbxFilterByLevel = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tslblSource = new System.Windows.Forms.ToolStripLabel();
            this.tscbxSites = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnLoadData = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnForward = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnBackUp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtnAddFunctions = new System.Windows.Forms.ToolStripDropDownButton();
            this.tssTDCalculator = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.flPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.tShowDateTime = new System.Windows.Forms.Timer(this.components);
            this.tAupBackground = new System.Windows.Forms.Timer(this.components);
            this.stspState.SuspendLayout();
            this.tspFunctions.SuspendLayout();
            this.SuspendLayout();
            // 
            // stspState
            // 
            this.stspState.BackColor = System.Drawing.Color.Thistle;
            this.stspState.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.stspState.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tstlblTime,
            this.tstlblNow,
            this.toolStripStatusLabel1,
            this.tsslblTmppage,
            this.tsspage,
            this.tssSpace,
            this.tssLastUpdated,
            this.tssupdatedTimestr});
            this.stspState.Location = new System.Drawing.Point(0, 423);
            this.stspState.Name = "stspState";
            this.stspState.Size = new System.Drawing.Size(799, 22);
            this.stspState.TabIndex = 0;
            // 
            // tstlblTime
            // 
            this.tstlblTime.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tstlblTime.Font = new System.Drawing.Font("微软雅黑 Light", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tstlblTime.ForeColor = System.Drawing.Color.Magenta;
            this.tstlblTime.Name = "tstlblTime";
            this.tstlblTime.Size = new System.Drawing.Size(74, 17);
            this.tstlblTime.Text = "NowTime：";
            // 
            // tstlblNow
            // 
            this.tstlblNow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tstlblNow.Font = new System.Drawing.Font("微软雅黑 Light", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tstlblNow.ForeColor = System.Drawing.Color.Magenta;
            this.tstlblNow.Name = "tstlblNow";
            this.tstlblNow.Size = new System.Drawing.Size(56, 17);
            this.tstlblNow.Text = "现在时间";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("微软雅黑 Light", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(207, 17);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // tsslblTmppage
            // 
            this.tsslblTmppage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsslblTmppage.Font = new System.Drawing.Font("微软雅黑 Light", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsslblTmppage.ForeColor = System.Drawing.Color.Red;
            this.tsslblTmppage.Name = "tsslblTmppage";
            this.tsslblTmppage.Size = new System.Drawing.Size(73, 17);
            this.tsslblTmppage.Text = "NowPage：";
            // 
            // tsspage
            // 
            this.tsspage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsspage.Font = new System.Drawing.Font("微软雅黑 Light", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsspage.ForeColor = System.Drawing.Color.Red;
            this.tsspage.Name = "tsspage";
            this.tsspage.Size = new System.Drawing.Size(35, 17);
            this.tsspage.Text = "0 / 0";
            // 
            // tssSpace
            // 
            this.tssSpace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssSpace.Font = new System.Drawing.Font("微软雅黑 Light", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tssSpace.Name = "tssSpace";
            this.tssSpace.Size = new System.Drawing.Size(207, 17);
            this.tssSpace.Spring = true;
            // 
            // tssLastUpdated
            // 
            this.tssLastUpdated.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssLastUpdated.Font = new System.Drawing.Font("微软雅黑 Light", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tssLastUpdated.ForeColor = System.Drawing.Color.Maroon;
            this.tssLastUpdated.Name = "tssLastUpdated";
            this.tssLastUpdated.Size = new System.Drawing.Size(82, 17);
            this.tssLastUpdated.Text = "Last Updated:";
            // 
            // tssupdatedTimestr
            // 
            this.tssupdatedTimestr.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tssupdatedTimestr.ForeColor = System.Drawing.Color.Fuchsia;
            this.tssupdatedTimestr.Name = "tssupdatedTimestr";
            this.tssupdatedTimestr.Size = new System.Drawing.Size(49, 17);
            this.tssupdatedTimestr.Text = "update";
            // 
            // tspFunctions
            // 
            this.tspFunctions.BackColor = System.Drawing.Color.Thistle;
            this.tspFunctions.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tspFunctions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnInspect,
            this.toolStripSeparator1,
            this.tsbtnCondtions,
            this.toolStripSeparator2,
            this.tsbtnShowSet,
            this.toolStripSeparator3,
            this.tscbxFilterByLevel,
            this.toolStripSeparator4,
            this.tslblSource,
            this.tscbxSites,
            this.toolStripSeparator5,
            this.toolStripSeparator6,
            this.tsbtnLoadData,
            this.toolStripSeparator7,
            this.toolStripSeparator8,
            this.tsbtnForward,
            this.toolStripSeparator9,
            this.toolStripSeparator10,
            this.tsbtnBackUp,
            this.toolStripSeparator11,
            this.toolStripSeparator12,
            this.tsbtnCopy,
            this.toolStripSeparator13,
            this.toolStripSeparator14,
            this.tsBtnAddFunctions,
            this.toolStripSeparator16});
            this.tspFunctions.Location = new System.Drawing.Point(0, 0);
            this.tspFunctions.Name = "tspFunctions";
            this.tspFunctions.Size = new System.Drawing.Size(799, 25);
            this.tspFunctions.TabIndex = 1;
            this.tspFunctions.Text = "toolStrip1";
            // 
            // tsbtnInspect
            // 
            this.tsbtnInspect.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnInspect.Image = global::FlashNDCollection.Properties.Resources.sun;
            this.tsbtnInspect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnInspect.Name = "tsbtnInspect";
            this.tsbtnInspect.Size = new System.Drawing.Size(76, 22);
            this.tsbtnInspect.Text = "检除格式";
            this.tsbtnInspect.Click += new System.EventHandler(this.tsbtnInspect_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnCondtions
            // 
            this.tsbtnCondtions.Image = global::FlashNDCollection.Properties.Resources.locate;
            this.tsbtnCondtions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCondtions.Name = "tsbtnCondtions";
            this.tsbtnCondtions.Size = new System.Drawing.Size(52, 22);
            this.tsbtnCondtions.Text = "条件";
            this.tsbtnCondtions.Click += new System.EventHandler(this.tsbtnContions_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnShowSet
            // 
            this.tsbtnShowSet.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnShowSet.Image = global::FlashNDCollection.Properties.Resources.vision;
            this.tsbtnShowSet.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnShowSet.Name = "tsbtnShowSet";
            this.tsbtnShowSet.Size = new System.Drawing.Size(52, 22);
            this.tsbtnShowSet.Text = "快讯";
            this.tsbtnShowSet.Click += new System.EventHandler(this.tsbtnShowSet_Click);
            this.tsbtnShowSet.TextChanged += new System.EventHandler(this.tsbtnShowSet_TextChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tscbxFilterByLevel
            // 
            this.tscbxFilterByLevel.BackColor = System.Drawing.Color.Thistle;
            this.tscbxFilterByLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbxFilterByLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tscbxFilterByLevel.ForeColor = System.Drawing.Color.Red;
            this.tscbxFilterByLevel.IntegralHeight = false;
            this.tscbxFilterByLevel.Items.AddRange(new object[] {
            "全部显示",
            "★★★★★",
            "★★★★",
            "★★★",
            "★★",
            "★"});
            this.tscbxFilterByLevel.Name = "tscbxFilterByLevel";
            this.tscbxFilterByLevel.Size = new System.Drawing.Size(75, 25);
            this.tscbxFilterByLevel.SelectedIndexChanged += new System.EventHandler(this.tscbxFilterByLevel_SelectedIndexChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tslblSource
            // 
            this.tslblSource.Name = "tslblSource";
            this.tslblSource.Size = new System.Drawing.Size(44, 22);
            this.tslblSource.Text = "来源：";
            // 
            // tscbxSites
            // 
            this.tscbxSites.BackColor = System.Drawing.Color.Thistle;
            this.tscbxSites.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbxSites.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tscbxSites.ForeColor = System.Drawing.Color.Magenta;
            this.tscbxSites.Name = "tscbxSites";
            this.tscbxSites.Size = new System.Drawing.Size(85, 25);
            this.tscbxSites.SelectedIndexChanged += new System.EventHandler(this.tscbxSites_SelectedIndexChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnLoadData
            // 
            this.tsbtnLoadData.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnLoadData.Image = global::FlashNDCollection.Properties.Resources.rotate;
            this.tsbtnLoadData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnLoadData.Name = "tsbtnLoadData";
            this.tsbtnLoadData.Size = new System.Drawing.Size(76, 22);
            this.tsbtnLoadData.Text = "加载快讯";
            this.tsbtnLoadData.Click += new System.EventHandler(this.tsbtnLoadData_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnForward
            // 
            this.tsbtnForward.Image = global::FlashNDCollection.Properties.Resources.next;
            this.tsbtnForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnForward.Name = "tsbtnForward";
            this.tsbtnForward.Size = new System.Drawing.Size(64, 22);
            this.tsbtnForward.Text = "下一页";
            this.tsbtnForward.Click += new System.EventHandler(this.tsbtnForward_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnBackUp
            // 
            this.tsbtnBackUp.Image = global::FlashNDCollection.Properties.Resources.previous;
            this.tsbtnBackUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnBackUp.Name = "tsbtnBackUp";
            this.tsbtnBackUp.Size = new System.Drawing.Size(64, 22);
            this.tsbtnBackUp.Text = "上一页";
            this.tsbtnBackUp.Click += new System.EventHandler(this.tsbtnBackUp_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbtnCopy
            // 
            this.tsbtnCopy.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnCopy.Image = global::FlashNDCollection.Properties.Resources.images;
            this.tsbtnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCopy.Name = "tsbtnCopy";
            this.tsbtnCopy.Size = new System.Drawing.Size(76, 22);
            this.tsbtnCopy.Text = "批量复制";
            this.tsbtnCopy.Click += new System.EventHandler(this.tsbtnCopy_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 25);
            // 
            // tsBtnAddFunctions
            // 
            this.tsBtnAddFunctions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnAddFunctions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssTDCalculator});
            this.tsBtnAddFunctions.Image = global::FlashNDCollection.Properties.Resources.star;
            this.tsBtnAddFunctions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnAddFunctions.Name = "tsBtnAddFunctions";
            this.tsBtnAddFunctions.Size = new System.Drawing.Size(29, 22);
            this.tsBtnAddFunctions.Text = "扩展";
            // 
            // tssTDCalculator
            // 
            this.tssTDCalculator.Image = ((System.Drawing.Image)(resources.GetObject("tssTDCalculator.Image")));
            this.tssTDCalculator.Name = "tssTDCalculator";
            this.tssTDCalculator.Size = new System.Drawing.Size(152, 22);
            this.tssTDCalculator.Text = "TD开盘计算器";
            this.tssTDCalculator.Click += new System.EventHandler(this.tssTDCalculator_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 25);
            // 
            // flPanel
            // 
            this.flPanel.AutoScroll = true;
            this.flPanel.BackColor = System.Drawing.Color.White;
            this.flPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flPanel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.flPanel.Location = new System.Drawing.Point(0, 25);
            this.flPanel.Name = "flPanel";
            this.flPanel.Size = new System.Drawing.Size(799, 398);
            this.flPanel.TabIndex = 2;
            // 
            // tShowDateTime
            // 
            this.tShowDateTime.Tick += new System.EventHandler(this.tShowDateTime_Tick);
            // 
            // tAupBackground
            // 
            this.tAupBackground.Tick += new System.EventHandler(this.tAupBackground_Tick);
            // 
            // MainFormNewsCollection
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(799, 445);
            this.Controls.Add(this.flPanel);
            this.Controls.Add(this.tspFunctions);
            this.Controls.Add(this.stspState);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(815, 768);
            this.MinimumSize = new System.Drawing.Size(815, 152);
            this.Name = "MainFormNewsCollection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "快讯采集 V1.6.5";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.MainFormNewsCollection_Load);
            this.Resize += new System.EventHandler(this.MainFormNewsCollection_Resize);
            this.stspState.ResumeLayout(false);
            this.stspState.PerformLayout();
            this.tspFunctions.ResumeLayout(false);
            this.tspFunctions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.StatusStrip stspState;
        private System.Windows.Forms.ToolStrip tspFunctions;
        private System.Windows.Forms.ToolStripButton tsbtnInspect;
        private System.Windows.Forms.ToolStripButton tsbtnCondtions;
        private System.Windows.Forms.ToolStripButton tsbtnShowSet;
        private System.Windows.Forms.ToolStripLabel tslblSource;
        private System.Windows.Forms.ToolStripComboBox tscbxSites;
        private System.Windows.Forms.ToolStripButton tsbtnLoadData;
        private System.Windows.Forms.ToolStripButton tsbtnForward;
        private System.Windows.Forms.ToolStripButton tsbtnBackUp;
        private System.Windows.Forms.ToolStripButton tsbtnCopy;
        private System.Windows.Forms.ToolStripDropDownButton tsBtnAddFunctions;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.FlowLayoutPanel flPanel;
        private System.Windows.Forms.ToolStripStatusLabel tstlblTime;
        private System.Windows.Forms.ToolStripStatusLabel tstlblNow;
        private System.Windows.Forms.Timer tShowDateTime;
        private System.Windows.Forms.ToolStripStatusLabel tsslblTmppage;
        private System.Windows.Forms.ToolStripStatusLabel tssLastUpdated;
        private System.Windows.Forms.ToolStripMenuItem tssTDCalculator;
        private System.Windows.Forms.ToolStripStatusLabel tsspage;
        private System.Windows.Forms.ToolStripStatusLabel tssSpace;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripComboBox tscbxFilterByLevel;
        private System.Windows.Forms.ToolStripStatusLabel tssupdatedTimestr;
        private System.Windows.Forms.Timer tAupBackground;
    }
}

#endregion