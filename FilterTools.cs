﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FlashNDCollection
{
    static class FilterTools
    {
        static FilterTools()
        {
            KeyWordsList = readRemovingKeywordsFromFile();
        }
        public static int RefalshSpan { get; set; } = 8;

        public static bool WhetherAutoUpdate { get; set; } = true;

        //显示新闻 or 数据
        private static bool showNewsOrData = true;

        public static bool ShowNewsOrData
        {
            get { return showNewsOrData; }
            set { showNewsOrData = value; }
        }

        //是否启用只读
        private static bool isReadOnly = false;

        public static bool IsReadOnly
        {
            get { return isReadOnly; }
            set { isReadOnly = value; }
        }
        //每页显示条数
        private static int perPageMaxResult = 6;

        public static int PerPageMaxResult
        {
            get { return perPageMaxResult; }
            set { perPageMaxResult = value; }
        }

        //设置等级显示
        private static int level = 0;

        public static int Level
        {
            get { return level; }
            set { level = value; }
        }

        //关键字
        private static List<string> keyWordslist;
        public static List<string> KeyWordsList
        {
            get { return keyWordslist; }
            set { keyWordslist = value; }
        }


        //去关键字方法

        public static string removeKeywords(string str)
        {
            if (KeyWordsList!= null)
            {
                foreach (var item in KeyWordsList)
                {
                    if (item != null && item.Trim() != "")
                    {
                        str = str.Replace(item, "");
                    }
                }
            }
            return str.Trim();
        }

        //其他工具
        public static string catchStr(string str, string start, string end)
        {
            string regpattern = string.Format(@"(?<={0}).*(?={1})", start, end);
            Regex rgx = new Regex(regpattern);
            return rgx.Match(str).Value;
        }

        public static string regexData(string str, string regxstr)
        {
            Regex rgx = new Regex(regxstr);
            str = rgx.Match(str).Value;
            return str;
        }
        public static string trimAll(string str)
        {
            str = str.Replace(" ", "");
            str = str.Replace("\r", "");
            str = str.Replace("\n", "");
            str = str.Replace("\t", "");
            return str;
        }

        //从文件读取keywords
        private static List<string> readRemovingKeywordsFromFile()
        {
            List<string> wordslist = new List<string>();
            if (File.Exists("RWordlist"))
            {
                using (StreamReader sr = new StreamReader("RWordlist", Encoding.Default))
                {
                    string tmpword = "";
                    while ((tmpword = sr.ReadLine()) != null)
                    {
                        if (tmpword.Trim() != "")
                        {
                            wordslist.Add(tmpword);
                        }
                    }
                }
            }
            return wordslist;
        }

        // 时间戳转为C#格式时间
        public static DateTime StampToDateTime(string timeStamp)
        {
            DateTime dateTimeStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime);

            return dateTimeStart.Add(toNow);
        }

        // DateTime时间格式转换为Unix时间戳格式
        public static int DateTimeToStamp(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int)(time - startTime).TotalSeconds;
        }
    }
}
