﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FlashNDCollection
{
    public partial class FormConditionFormats : Form
    {
        public FormConditionFormats()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSure_Click(object sender, EventArgs e)
        {
            FilterTools.PerPageMaxResult = int.Parse(numShowing.Value.ToString());
            FilterTools.KeyWordsList = stringTolist(txtKeyWords.Text);
            updateRemovingKeywordsToFile(FilterTools.KeyWordsList);
            FilterTools.IsReadOnly = cbxSetWritatble.Checked;
            FilterTools.RefalshSpan = int.Parse(numtimespan.Value.ToString());
            if (cbxReflashSpan.Checked == true)
            {
                FilterTools.WhetherAutoUpdate = true;
            }
            else
            {
                FilterTools.WhetherAutoUpdate = false;
            }
            
            Close();
        }

        private void FormConditionFormats_Load(object sender, EventArgs e)
        {
            numtimespan.Enabled = false;
            numShowing.Value = decimal.Parse( FilterTools.PerPageMaxResult.ToString());
            txtKeyWords.Text = listTostring(FilterTools.KeyWordsList);
            cbxSetWritatble.Checked = FilterTools.IsReadOnly;

            cbxReflashSpan.Checked = FilterTools.WhetherAutoUpdate;
            numtimespan.Value = decimal.Parse( FilterTools.RefalshSpan.ToString());
        }

        private string listTostring(List<string> strlist)
        {
            string tmp = "";
            if (strlist != null)
            {

                foreach (var item in strlist)
                {
                    tmp = tmp + item + "|";
                }
            }
            return tmp;
        }

        private List<string> stringTolist(string str)
        {
            List<string> strlist = new List<string>();

            string[] strArray = str.Split('|');
            if (strArray != null)
            { 
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (strArray[i] != null && strArray[i].Trim() != "")
                    {
                        strlist.Add(strArray[i]);
                    }
                }
            }
            return strlist;
        }

        private void cbxSetWritatble_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxSetWritatble.Checked == true)
            {
                FilterTools.IsReadOnly = false;
            }
            else
            {
                FilterTools.IsReadOnly = true;
            }
        }

        private void cbxReflashSpan_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxReflashSpan.Checked == true)
            {
                numtimespan.Enabled = true;
            }
            else
            {
                numtimespan.Enabled = false;
            }
        }

        //加载需要去除的关键字
        private List<string> readRemovingKeywordsFromFile()
        {
            List<string> wordslist = new List<string>();
            if (File.Exists("RWordlist"))
            {
                using (StreamReader sr = new StreamReader("RWordlist", Encoding.Default))
                {
                    string tmpword = "";
                    while ((tmpword = sr.ReadLine()) != null)
                    {
                        if (tmpword.Trim() != "")
                        {
                            wordslist.Add(tmpword);
                        }                        
                    }
                }                
            }
            return wordslist;
        }

        //写入新的去除关键字
        private void updateRemovingKeywordsToFile(List<string> wordlist)
        {
            if (wordlist != null)
            {
                if (File.Exists("RWordlist"))
                {
                    if (File.Exists("RWordlist.bak"))
                    {
                        File.Delete("RWordlist.bak");
                    }
                    File.Copy("RWordlist", "RWordlist.bak");
                }
                using (FileStream fs = new FileStream("RWordlist", FileMode.Create))
                {
                    using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        foreach (string words in wordlist)
                        {
                            if (words.Trim() != "")
                            {
                                sw.WriteLine(words);
                            }                            
                        }
                    }
                }
            }
        }
    }
}
