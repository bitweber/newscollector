﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlashNDCollection
{
    interface ICatchFlashNews
    {
        List<FlashNewsObject> getFlashNewsList();
    }
}
