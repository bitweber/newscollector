﻿namespace FlashNDCollection
{
    partial class FormConditionFormats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConditionFormats));
            this.lblPageNums = new System.Windows.Forms.Label();
            this.numShowing = new System.Windows.Forms.NumericUpDown();
            this.grpBaseConditions = new System.Windows.Forms.GroupBox();
            this.numtimespan = new System.Windows.Forms.NumericUpDown();
            this.cbxReflashSpan = new System.Windows.Forms.CheckBox();
            this.cbxSetWritatble = new System.Windows.Forms.CheckBox();
            this.grpHighConditions = new System.Windows.Forms.GroupBox();
            this.txtKeyWords = new System.Windows.Forms.TextBox();
            this.btnSure = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numShowing)).BeginInit();
            this.grpBaseConditions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numtimespan)).BeginInit();
            this.grpHighConditions.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPageNums
            // 
            this.lblPageNums.AutoSize = true;
            this.lblPageNums.Location = new System.Drawing.Point(11, 20);
            this.lblPageNums.Name = "lblPageNums";
            this.lblPageNums.Size = new System.Drawing.Size(68, 17);
            this.lblPageNums.TabIndex = 0;
            this.lblPageNums.Text = "每页条数：";
            // 
            // numShowing
            // 
            this.numShowing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numShowing.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numShowing.Location = new System.Drawing.Point(85, 19);
            this.numShowing.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.numShowing.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numShowing.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numShowing.Name = "numShowing";
            this.numShowing.Size = new System.Drawing.Size(50, 19);
            this.numShowing.TabIndex = 1;
            this.numShowing.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numShowing.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // grpBaseConditions
            // 
            this.grpBaseConditions.Controls.Add(this.numtimespan);
            this.grpBaseConditions.Controls.Add(this.cbxReflashSpan);
            this.grpBaseConditions.Controls.Add(this.cbxSetWritatble);
            this.grpBaseConditions.Controls.Add(this.numShowing);
            this.grpBaseConditions.Controls.Add(this.lblPageNums);
            this.grpBaseConditions.Location = new System.Drawing.Point(12, 4);
            this.grpBaseConditions.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpBaseConditions.Name = "grpBaseConditions";
            this.grpBaseConditions.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpBaseConditions.Size = new System.Drawing.Size(209, 77);
            this.grpBaseConditions.TabIndex = 2;
            this.grpBaseConditions.TabStop = false;
            this.grpBaseConditions.Text = "基本显示";
            // 
            // numtimespan
            // 
            this.numtimespan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numtimespan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numtimespan.Location = new System.Drawing.Point(131, 47);
            this.numtimespan.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.numtimespan.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numtimespan.Name = "numtimespan";
            this.numtimespan.Size = new System.Drawing.Size(64, 19);
            this.numtimespan.TabIndex = 4;
            this.numtimespan.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numtimespan.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // cbxReflashSpan
            // 
            this.cbxReflashSpan.AutoSize = true;
            this.cbxReflashSpan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxReflashSpan.Location = new System.Drawing.Point(14, 46);
            this.cbxReflashSpan.Name = "cbxReflashSpan";
            this.cbxReflashSpan.Size = new System.Drawing.Size(111, 21);
            this.cbxReflashSpan.TabIndex = 3;
            this.cbxReflashSpan.Text = "自动刷新（秒）:";
            this.cbxReflashSpan.UseVisualStyleBackColor = true;
            this.cbxReflashSpan.CheckedChanged += new System.EventHandler(this.cbxReflashSpan_CheckedChanged);
            // 
            // cbxSetWritatble
            // 
            this.cbxSetWritatble.AutoSize = true;
            this.cbxSetWritatble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxSetWritatble.Location = new System.Drawing.Point(147, 18);
            this.cbxSetWritatble.Name = "cbxSetWritatble";
            this.cbxSetWritatble.Size = new System.Drawing.Size(48, 21);
            this.cbxSetWritatble.TabIndex = 2;
            this.cbxSetWritatble.Text = "只读";
            this.cbxSetWritatble.UseVisualStyleBackColor = true;
            this.cbxSetWritatble.CheckedChanged += new System.EventHandler(this.cbxSetWritatble_CheckedChanged);
            // 
            // grpHighConditions
            // 
            this.grpHighConditions.Controls.Add(this.txtKeyWords);
            this.grpHighConditions.ForeColor = System.Drawing.Color.Red;
            this.grpHighConditions.Location = new System.Drawing.Point(12, 88);
            this.grpHighConditions.Name = "grpHighConditions";
            this.grpHighConditions.Size = new System.Drawing.Size(209, 86);
            this.grpHighConditions.TabIndex = 3;
            this.grpHighConditions.TabStop = false;
            this.grpHighConditions.Text = "复制去关键字（以“/”隔开）";
            // 
            // txtKeyWords
            // 
            this.txtKeyWords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtKeyWords.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtKeyWords.Location = new System.Drawing.Point(6, 21);
            this.txtKeyWords.Multiline = true;
            this.txtKeyWords.Name = "txtKeyWords";
            this.txtKeyWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtKeyWords.Size = new System.Drawing.Size(197, 55);
            this.txtKeyWords.TabIndex = 0;
            // 
            // btnSure
            // 
            this.btnSure.BackColor = System.Drawing.Color.Silver;
            this.btnSure.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSure.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSure.Location = new System.Drawing.Point(29, 180);
            this.btnSure.Name = "btnSure";
            this.btnSure.Size = new System.Drawing.Size(81, 23);
            this.btnSure.TabIndex = 4;
            this.btnSure.Text = "确定";
            this.btnSure.UseVisualStyleBackColor = false;
            this.btnSure.Click += new System.EventHandler(this.btnSure_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Silver;
            this.btnExit.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnExit.Location = new System.Drawing.Point(124, 180);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(81, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "取消";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // FormConditionFormats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(233, 209);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSure);
            this.Controls.Add(this.grpHighConditions);
            this.Controls.Add(this.grpBaseConditions);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormConditionFormats";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "条件";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormConditionFormats_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numShowing)).EndInit();
            this.grpBaseConditions.ResumeLayout(false);
            this.grpBaseConditions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numtimespan)).EndInit();
            this.grpHighConditions.ResumeLayout(false);
            this.grpHighConditions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPageNums;
        private System.Windows.Forms.NumericUpDown numShowing;
        private System.Windows.Forms.GroupBox grpBaseConditions;
        private System.Windows.Forms.GroupBox grpHighConditions;
        private System.Windows.Forms.Button btnSure;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtKeyWords;
        private System.Windows.Forms.CheckBox cbxSetWritatble;
        private System.Windows.Forms.NumericUpDown numtimespan;
        private System.Windows.Forms.CheckBox cbxReflashSpan;
    }
}