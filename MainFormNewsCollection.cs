﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace FlashNDCollection
{
    public partial class MainFormNewsCollection : Form
    {
        public MainFormNewsCollection()
        {
            InitializeComponent();
        }

        ItemBox itemBox = new ItemBox();

        List<DataObject> localDataList;
        List<FlashNewsObject> localNewsFlashList;

        ICatchData dataCollection;
        ICatchFlashNews flashNewsCollection;

        int tmpPage = 1;
        int totalPage = 0;

        int pageMaxResults = 6;
        string activedSiteName = "金十数据";

        //private void initBaseSettings()
        //{
        //    FilterTools.RefalshSpan = 3;
        //    FilterTools.PerPageMaxResult = 6;
        //    FilterTools.WhetherAutoUpdate = true;
        //    FilterTools.IsReadOnly = false;
        //    FilterTools.ShowNewsOrData = true;
        //    FilterTools.Level = 0;
        //}

        //form load 事件
        private async void MainFormNewsCollection_Load(object sender, EventArgs e)
        {
            try
            {
                //加载基本的配置
                pageMaxResults = FilterTools.PerPageMaxResult;
                setMaxWindowSize();
                tShowDateTime.Interval = 1000;
                tShowDateTime.Enabled = true;
                tstlblNow.Text = DateTime.Now.ToString();
                tssupdatedTimestr.Text = DateTime.Now.ToString();
                tscbxFilterByLevel.SelectedIndex = 0;
                addSiteSources();
                activedSiteName = tscbxSites.Text;
                await Task.Factory.StartNew(()=> reflashContent(activedSiteName));
                showAllItemsOnPage(tmpPage, totalPage, pageMaxResults, localNewsFlashList, localDataList, FilterTools.ShowNewsOrData);
                setAutoUpdateState();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!");
            }finally
            {
                showPage();
                StatusSettings.SelectDatas = "";//每次刷新页码就清空选中值
            }
        }

        //添加来源网站
        private void addSiteSources()
        {
            if (FilterTools.ShowNewsOrData == true)
            {
                tscbxSites.Items.Clear();
                tscbxSites.Items.Add("金十数据");
                tscbxSites.Items.Add("汇通快讯");
                tscbxSites.Items.Add("华尔街见闻");
                tscbxSites.SelectedIndex = 0;
            }
            else
            {
                tscbxSites.Items.Clear();
                tscbxSites.Items.Add("金十数据");
                //tscbxSites.Items.Add("汇通快讯");
                tscbxSites.SelectedIndex = 0;
            }
        }

        //设置单个item的值
        private void setItemBox(string time, string data, int level = 0)
        {       
            itemBox = new ItemBox();
            itemBox.set(time, data, level);

            if (flPanel.InvokeRequired)
            {
                flPanel.Invoke((MethodInvoker)delegate ()
                {
                    flPanel.Controls.Add(itemBox);
                });
            }
            else
            {
                flPanel.Controls.Add(itemBox);
            }
        }

        //清空控件
        private void clearItems()
        {
            if (flPanel.InvokeRequired)
            {
                flPanel.Invoke((MethodInvoker)delegate () {
                    flPanel.Controls.Clear();//清空所有控件
                });
            }
            else
            {
                flPanel.Controls.Clear();//清空所有控件
            }
        }

        //显示页面
        private async void showAllItemsOnPage(int tmppage, int totalpage, int pagemaxresults, List<FlashNewsObject> newslist,List<DataObject> datalist,bool newsordata)
        {
            if (newsordata == true)
            {
                await Task.Factory.StartNew(() => addAllNewsItems(tmppage, totalpage, pagemaxresults, newslist));
            }
            else
            {
                await Task.Factory.StartNew(() => addAllDataItems(tmppage, totalpage, pagemaxresults, datalist));
            }
            StatusSettings.SelectDatas = "";//每次刷新页码就清空选中值
        }

        //新闻添加
        private async void addAllNewsItems(int tmppage, int totalpage, int pagemaxresults, List<FlashNewsObject> newslist)
        {
            if (tmppage > 0 && totalpage > 0 && tmppage <= totalpage && newslist !=null)
            {
                //flPanel.Controls.Clear();//开始前清空所有控件
                await Task.Factory.StartNew(() => clearItems());
                if (tmppage < totalpage)
                {
                    for (int i = (tmppage - 1) * pagemaxresults; i < tmppage * pagemaxresults; i++)
                    {
                        string tmpstr = string.Format("{0}", newslist[i].News);
                        await Task.Factory.StartNew(() => setItemBox(newslist[i].Time.ToLongTimeString(), tmpstr, newslist[i].Level));
                    }
                }
                else
                {
                    for (int i = (tmppage - 1) * pagemaxresults; i < newslist.Count; i++)
                    {
                        string tmpstr = string.Format("{0}", newslist[i].News);
                        await Task.Factory.StartNew(()=> setItemBox(newslist[i].Time.ToLongTimeString(), tmpstr, newslist[i].Level));
                    }
                }
            }
            else
            {
                //flPanel.Controls.Clear();//清空所有控件
                await Task.Factory.StartNew(()=> clearItems());
                if (FilterTools.WhetherAutoUpdate == true)
                {
                    FilterTools.WhetherAutoUpdate = false;
                    //MessageBox.Show("加载失败，没有快讯！", "错误提示：", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    showDataError();
                    FilterTools.WhetherAutoUpdate = true;
                }
                else
                {
                    //MessageBox.Show("加载失败，没有快讯！", "错误提示：", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    showDataError();
                }
                
            }
        }

        //数据添加
        private async void addAllDataItems(int tmppage,int totalpage,int pagemaxresults,List<DataObject> datalist)
        {
            if (tmppage > 0 && totalpage > 0 && tmppage <= totalpage && datalist !=null)
            {
                //flPanel.Controls.Clear();//清空所有控件
                await Task.Factory.StartNew(()=> clearItems());
                if (tmppage < totalpage)
                {
                    for (int i = (tmppage - 1) * pagemaxresults; i < tmppage * pagemaxresults; i++)
                    {
                        if (datalist[i].Expectedvalue != null && datalist[i].Expectedvalue != "")
                        {
                            string tmpstr = string.Format("{0}实际值为{1}，{2}。", datalist[i].DataName, datalist[i].ActualValue, datalist[i].AcPreCompare);
                            if (datalist[i].Level >= 4 && datalist[i].DataAffacts!=null && (!datalist[i].DataAffacts.Contains("影响较小")))
                            {
                                tmpstr = string.Format("{0}实际值为{1}，{2}，{3}。", datalist[i].DataName, datalist[i].ActualValue, datalist[i].AcPreCompare, datalist[i].DataAffacts);
                            }
                            await Task.Factory.StartNew(()=> setItemBox(datalist[i].Time.ToLongTimeString(), tmpstr, datalist[i].Level));
                        }
                        else
                        {
                            string tmpstr = string.Format("{0}实际值为{1}。", datalist[i].DataName, datalist[i].ActualValue);
                            await Task.Factory.StartNew(()=> setItemBox(datalist[i].Time.ToLongTimeString(), tmpstr, datalist[i].Level));
                        }                       
                    }
                }
                else
                {
                    for (int i = (tmppage-1)* pagemaxresults; i < datalist.Count; i++)
                    {
                        if (datalist[i].Expectedvalue != null && datalist[i].Expectedvalue != "")
                        {
                            string tmpstr = string.Format("{0}实际值为{1}，{2}。", datalist[i].DataName, datalist[i].ActualValue, datalist[i].AcPreCompare);
                            if (datalist[i].Level >= 4 && datalist[i].DataAffacts != null && (!datalist[i].DataAffacts.Contains("影响较小")))
                            {
                                tmpstr = string.Format("{0}实际值为{1}，{2}，{3}。", datalist[i].DataName, datalist[i].ActualValue, datalist[i].AcPreCompare, datalist[i].DataAffacts);
                            }
                            await Task.Factory.StartNew(()=> setItemBox(datalist[i].Time.ToLongTimeString(), tmpstr, datalist[i].Level));
                        }
                        else
                        {
                            string tmpstr = string.Format("{0}实际值为{1}。", datalist[i].DataName, datalist[i].ActualValue);
                            await Task.Factory.StartNew(()=> setItemBox(datalist[i].Time.ToLongTimeString(), tmpstr, datalist[i].Level));
                        }
                    }
                }                
            }
            else
            {
                //flPanel.Controls.Clear();//清空所有控件
                await Task.Factory.StartNew(()=> clearItems());
                if (FilterTools.WhetherAutoUpdate == true)
                {
                    FilterTools.WhetherAutoUpdate = false;
                    //MessageBox.Show("加载失败，没有数据！", "错误提示：", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    showDataError();
                    FilterTools.WhetherAutoUpdate = true;
                }
                else
                {
                    //MessageBox.Show("加载失败，没有数据！", "错误提示：", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    showDataError();
                }
                
            }
        }

        //刷新内容
        private void reflashContent(string sitename)
        {
            try
            {
                if (FilterTools.ShowNewsOrData == true)
                {
                    switch (sitename)
                    {
                        case "华尔街见闻":
                            flashNewsCollection = new WallStreetNewsCollection();
                            break;
                        case "汇通快讯":
                            flashNewsCollection = new HuiTongNewsCollection();
                            break;
                        case "金十数据":
                            flashNewsCollection = new JinShiNewsCollection();
                            break;
                    }
                    localNewsFlashList = new List<FlashNewsObject>(flashNewsCollection.getFlashNewsList());
                    totalPage = calTotalPageNums(localNewsFlashList, pageMaxResults);
                }
                else
                {
                    switch (sitename)
                    {
                        case "汇通快讯":
                            dataCollection = new HuiTongNewsCollection();
                            break;
                        case "金十数据":
                            dataCollection = new JinShiNewsCollection();
                            break;
                    }
                    localDataList = new List<DataObject>(dataCollection.getDataList());                   
                    totalPage = calTotalPageNums(localDataList, pageMaxResults);              
                }
                if (totalPage>0)
                {
                    tmpPage = 1;
                }
        }catch (Exception ex)
        {
                if (FilterTools.WhetherAutoUpdate ==true)
                {
                    FilterTools.WhetherAutoUpdate = false;
                    if (ex.Message != "404" )
                    {
                        MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                    
                    FilterTools.WhetherAutoUpdate = true;
                }
                else
                {
                    if (ex.Message != "404")
                    {
                        MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                }        
        }
}

        //计算总页码数
        private int calTotalPageNums<T>(List<T> t,int maxresult)
        {
            if (t!=null && maxresult != 0)
            {
                return (t.Count + maxresult - 1) / maxresult;
            }
            else
            {
                return 0;
            }
        }

        //显示当前页码
        private void showPage()
        {
            //if (stspState.InvokeRequired)
            //{
            //    stspState.Invoke((MethodInvoker)delegate () { tsspage.Text = string.Format("{0} / {1}", nowpage.ToString(), totalpage.ToString()); });
            //}
            //else
            //{
            //    tsspage.Text = string.Format("{0} / {1}", nowpage.ToString(), totalpage.ToString());
            //}
            if (totalPage > 0 && tmpPage > 0)
            {
                tsspage.Text = string.Format("{0} / {1}", tmpPage.ToString(), totalPage.ToString());
            }
            else
            {
                tsspage.Text = string.Format("0 / 0");
            }
        }

        //首页数据
        private async void tsbtnLoadData_Click(object sender, EventArgs e)
        {
            try
            {
                tsbtnLoadData.Enabled = false;
                activedSiteName = tscbxSites.Text;
                pageMaxResults = FilterTools.PerPageMaxResult;
                await Task.Factory.StartNew(() => reflashContent(activedSiteName));
                showAllItemsOnPage(tmpPage, totalPage, pageMaxResults, localNewsFlashList, localDataList, FilterTools.ShowNewsOrData);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!",MessageBoxButtons.OKCancel,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1,MessageBoxOptions.DefaultDesktopOnly);
            }
            finally
            {
                showPage();
                tssupdatedTimestr.Text = DateTime.Now.ToString();
                reflashAutoUpdateState();
                tsbtnLoadData.Enabled = true;
            }
        }

        //下一页
        private async void tsbtnForward_Click(object sender, EventArgs e)
        {
            try
            {
                pageMaxResults = FilterTools.PerPageMaxResult;
                tsbtnForward.Enabled = false;
                StatusSettings.SelectDatas = "";//每次刷新页码就清空选中值
                if (FilterTools.ShowNewsOrData == true)
                {
                    totalPage = calTotalPageNums(localNewsFlashList, pageMaxResults);
                    if (tmpPage > 0 && tmpPage < totalPage)
                    {
                        tmpPage = tmpPage + 1;
                        await Task.Factory.StartNew(() => addAllNewsItems(tmpPage, totalPage, pageMaxResults, localNewsFlashList));                  
                    }
                    else
                    {
                        MessageBox.Show("已经到达尾页！", "警告：", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning,MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                }
                else
                {
                    totalPage = calTotalPageNums(localDataList, pageMaxResults);
                    if (tmpPage > 0 && tmpPage < totalPage)
                    {
                        tmpPage = tmpPage + 1;
                        await Task.Factory.StartNew(() => addAllDataItems(tmpPage, totalPage, pageMaxResults, localDataList));
                    }
                    else
                    {
                        MessageBox.Show("已经到达尾页！", "警告：", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"错误提示：" ,MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }finally
            {
                showPage();
                tsbtnForward.Enabled = true;
            }           
        }

        //上一页
        private async void tsbtnBackUp_Click(object sender, EventArgs e)
        {
            try
            {
                pageMaxResults = FilterTools.PerPageMaxResult;
                tsbtnBackUp.Enabled = false;
                StatusSettings.SelectDatas = "";//每次刷新页码就清空选中值

                if (FilterTools.ShowNewsOrData == true)
                {
                    totalPage = calTotalPageNums(localNewsFlashList, pageMaxResults);
                    if (tmpPage > 1 && tmpPage <= totalPage)
                    {
                        tmpPage = tmpPage - 1;
                        await Task.Factory.StartNew(() => addAllNewsItems(tmpPage, totalPage, pageMaxResults, localNewsFlashList));
                    }
                    else
                    {
                        MessageBox.Show("已经到达首页！", "警告：", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                }
                else
                {
                    totalPage = calTotalPageNums(localDataList, pageMaxResults);
                    if (tmpPage > 1 && tmpPage <= totalPage)
                    {
                        tmpPage = tmpPage - 1;
                        await Task.Factory.StartNew(() => addAllDataItems(tmpPage, totalPage, pageMaxResults, localDataList));
                    }
                    else
                    {
                        MessageBox.Show("已经到达首页！", "警告：", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "错误提示：", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
            finally
            {
                showPage();
                tsbtnBackUp.Enabled = true;
            }           
        }

        //条件显示筛选
        private void tsbtnContions_Click(object sender, EventArgs e)
        {
            FormConditionFormats formconditonformat = new FormConditionFormats();
            formconditonformat.ShowDialog();
            setAutoUpdateState();
            pageMaxResults = FilterTools.PerPageMaxResult;
        }

        //设置检除格式功能   
        private void tsbtnInspect_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                string str = Clipboard.GetText();
                if (FilterTools.KeyWordsList != null)
                {
                    str = FilterTools.removeKeywords(str);
                }
                str = str.TrimEnd('\n').TrimEnd('\r');
                if (str != null && str != "")
                {
                    Clipboard.Clear();
                    Clipboard.SetText(str);
                }
            }
        }

        //批量复制功能
        private void tsbtnCopy_Click(object sender, EventArgs e)
        {
            if (StatusSettings.SelectDatas != null && StatusSettings.SelectDatas != "")
            {
                string tmp = FilterTools.removeKeywords(StatusSettings.SelectDatas);
                tmp = tmp.TrimEnd('\n').TrimEnd('\r');
                if (tmp!=null && tmp!="")
                {
                    Clipboard.Clear();
                    Clipboard.SetText(tmp);
                }
                //Clipboard.SetText(StatusSettings.SelectDatas.TrimEnd('\n').TrimEnd('\r'));
            }
            else
            {
                StatusSettings.SelectDatas = "";
                MessageBox.Show("没有选中任何快讯或数据！", "警告：", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        //数据快讯切换
        private async void tsbtnShowSet_Click(object sender, EventArgs e)
        {
            try
            {
                pageMaxResults = FilterTools.PerPageMaxResult;
                tsbtnShowSet.Enabled = false;
                if (tsbtnShowSet.Text == "快讯")
                {
                    tsbtnShowSet.Text = "数据";
                    FilterTools.ShowNewsOrData = false;
                    addSiteSources();
                    activedSiteName = tscbxSites.Text;
                    if (localDataList == null || localDataList.Count <= 0 || localDataList[0].FromSite!= activedSiteName)
                    {
                        await Task.Factory.StartNew(() => reflashContent(activedSiteName));
                        reflashAutoUpdateState();
                    }
                    tmpPage = 1;
                    totalPage = calTotalPageNums(localDataList, pageMaxResults);
                }
                else
                {
                    tsbtnShowSet.Text = "快讯";
                    FilterTools.ShowNewsOrData = true;
                    addSiteSources();
                    activedSiteName = tscbxSites.Text;
                    if (localNewsFlashList == null || localNewsFlashList.Count <= 0 || localNewsFlashList[0].FromSite!=activedSiteName)
                    {
                        await Task.Factory.StartNew(() => reflashContent(activedSiteName));
                        reflashAutoUpdateState();
                    }
                    tmpPage = 1;
                    totalPage = calTotalPageNums(localNewsFlashList, pageMaxResults);
                }       
                showAllItemsOnPage(tmpPage, totalPage, pageMaxResults, localNewsFlashList, localDataList, FilterTools.ShowNewsOrData);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
            finally
            {
                StatusSettings.SelectDatas = "";//刷新页码就清空选中值
                tssupdatedTimestr.Text = DateTime.Now.ToString();                
                showPage();
                tsbtnShowSet.Enabled = true;
            }
        }

        //切换快讯、数据变化
        private void tsbtnShowSet_TextChanged(object sender, EventArgs e)
        {
            if (tsbtnShowSet.Text == "快讯")
            {
                tsbtnLoadData.Text = "加载快讯";
            }
            else
            {
                tsbtnLoadData.Text = "加载数据";               
            }
        }

        //显示系统时间
        private void tShowDateTime_Tick(object sender, EventArgs e)
        {
            tstlblNow.Text = DateTime.Now.ToString();          
        }

        //黄金白银TD开盘报价计算器
        private void tssTDCalculator_Click(object sender, EventArgs e)
        {
            FormTDCalculator tdCal = new FormTDCalculator();
            tdCal.Show();
        }

        //设置过滤等级
        private void tscbxFilterByLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tscbxFilterByLevel.Text)
            {
                case "★★★★★":
                    FilterTools.Level = 5;
                    break;
                case "★★★★":
                    FilterTools.Level = 4;
                    break;
                case "★★★":
                    FilterTools.Level = 3;
                    break;
                case "★★":
                    FilterTools.Level = 2;
                    break;
                case "★":
                    FilterTools.Level = 1;
                    break;
                default:
                    FilterTools.Level = 0;
                    break;
            }
        }

        //设置最大窗口的大小
        private void setMaxWindowSize()
        {
            Rectangle rect = Screen.GetWorkingArea(this);
            MaximumSize = new Size(815,rect.Height);          
        }

        //屏幕缩放的时候设置显示的条数
        private void MainFormNewsCollection_Resize(object sender, EventArgs e)
        {
            FilterTools.PerPageMaxResult = flPanel.Height / (itemBox.Height + 2);
        }

        //切换站点时自动刷新 
        private async void tscbxSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (activedSiteName != tscbxSites.Text)
                {
                    pageMaxResults = FilterTools.PerPageMaxResult;
                    StatusSettings.SelectDatas = "";//每次刷新页码就清空选中值
                    activedSiteName = tscbxSites.Text;
                    await Task.Factory.StartNew(() => reflashContent(activedSiteName));
                    tmpPage = 1;
                    showAllItemsOnPage(tmpPage, totalPage, pageMaxResults, localNewsFlashList, localDataList, FilterTools.ShowNewsOrData);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
            finally
            {
                tssupdatedTimestr.Text = DateTime.Now.ToString();
                reflashAutoUpdateState();
                showPage();
            }
            
        }

        //定时更新数据
        private async void tAupBackground_Tick(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized && FilterTools.WhetherAutoUpdate == true)
            {
                try
                {
                    pageMaxResults = FilterTools.PerPageMaxResult;
                    activedSiteName = tscbxSites.Text;
                    await Task.Factory.StartNew(() => reflashContent(activedSiteName));
                    tmpPage = 1;
                    showAllItemsOnPage(tmpPage, totalPage, pageMaxResults, localNewsFlashList, localDataList, FilterTools.ShowNewsOrData);
                    tssupdatedTimestr.Text = DateTime.Now.ToString();
                    showPage();
                }
                catch(Exception ex)
                {
                    FilterTools.WhetherAutoUpdate = false;
                    //MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    FilterTools.WhetherAutoUpdate = true;
                }
            }
        }

        //检查更新功能
        private void setAutoUpdateState()
        {
            if (FilterTools.WhetherAutoUpdate == true)
            {
                if (tAupBackground.Interval != FilterTools.RefalshSpan * 1000)
                {
                    tAupBackground.Interval = FilterTools.RefalshSpan * 1000;
                }
                tAupBackground.Enabled = true;
            }
            else
            {
                tAupBackground.Enabled = false;
            }
        }

        //刷新自动更新周期起点
        private void reflashAutoUpdateState()
        {
            if (tAupBackground.Enabled == true)
            {
                tAupBackground.Enabled = false;
                tAupBackground.Enabled = true;
            }
        }

        //提示数据不存在
        private void showDataError()
        {
            DataError error = new DataError();
            error.Dock = DockStyle.Top;
            clearItems();
            //flPanel.Controls.Add(error);
            if (flPanel.InvokeRequired)
            {
                flPanel.Invoke((MethodInvoker)delegate ()
                {
                    flPanel.Controls.Add(error);
                });
            }
            else
            {
                flPanel.Controls.Add(error);
            }
        }
    }
}
