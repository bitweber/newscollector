﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlashNDCollection
{
    public partial class ItemBox : UserControl
    {
        public ItemBox()
        {
            InitializeComponent();
        }

        private void SingleItemShow_Load(object sender, EventArgs e)
        {
            txtItemBox.ReadOnly = FilterTools.IsReadOnly;
            txtItemBox.BackColor = Color.White;
        }

        //设置各个控件里面的内容
        public void set(string timestr, string text, int level)
        {
            if (level >= 3)
            {
                txtItemBox.ForeColor = Color.Red;
                lblTime.ForeColor = Color.Red;
                txtItemBox.Font = new Font(txtItemBox.Font, FontStyle.Bold);
            }
            else if (level >= 2)
            {
                txtItemBox.ForeColor = Color.DeepPink;
                lblTime.ForeColor = Color.DeepPink;
                //txtData.Font = new Font(txtData.Font, FontStyle.Italic);
            }
            lblLevelStar.Text = showLevelStars(level);
            lblTime.Text = timestr;
            txtItemBox.Text = text;
        }

        //设置等级星号显示
        private string showLevelStars(int level = 1)
        {
            string stars = "";
            switch (level)
            {
                case 5:
                    stars = "★★★★★";
                    break;
                case 4:
                    stars = "★★★★";
                    break;
                case 3:
                    stars = "★★★";
                    break;
                case 2:
                    stars = "★★";
                    break;
                default:
                    stars = "★";
                    break;
            }
            return stars;
        }

        //选中选项时添加到复制内容中
        private void cbxSelect_CheckedChanged(object sender, EventArgs e)
        {
            //string tmp = FilterSettings.removeKeywords(FilterSettings.KeyWordsList, txtData.Text);
            if (cbxSelect.Checked == true)
            {
                StatusSettings.SelectDatas += txtItemBox.Text + "\r\n";
            }
            else
            {
                StatusSettings.SelectDatas = StatusSettings.SelectDatas.Replace(txtItemBox.Text + "\r\n", "");
            }
        }

        //单击时间区域自动复制
        private void lblTime_Click(object sender, EventArgs e)
        {
            if (txtItemBox.Text != null && txtItemBox.Text != "")
            {
                Clipboard.Clear();
                string tmp = FilterTools.removeKeywords(txtItemBox.Text);
                Clipboard.SetText(tmp.TrimEnd('\n').TrimEnd('\r'));
            }
        }
    }
}
