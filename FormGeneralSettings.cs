﻿using System;
using System.Windows.Forms;

namespace FlashNDCollection
{
    public partial class FormGeneralSettings : Form
    {
        public FormGeneralSettings()
        {
            InitializeComponent();
        }

        private string tmpKeyString = "";
        //private decimal tmpTimeSpan = 5;
        //private bool tmpRemoveKeyStrChecked = false;
        //取消键设置
        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        //确定键的设置
        private void btnSure_Click(object sender, EventArgs e)
        {
            //GeneralOptions.CopyToCliOrNot = cbxCTB.Checked;
            //GeneralOptions.AutoFlashOrNot = cbxFlashData.Checked;
            //GeneralOptions.UpdateTimeSpan = tmpTimeSpan;
            //GeneralOptions.SaveConfigOrNot = cbxSaveSetting.Checked;
            //GeneralOptions.AutoCatchClipboardOrNot = cbxCatchClipboard.Checked;
            //GeneralOptions.RemoveStrOrNot = tmpRemoveKeyStrChecked;
            //GeneralOptions.RemoveKeyString = tmpKeyString;
            Close();
        }

        //读取设置
        private void readSettings()
        {
            //cbxCTB.Checked = GeneralOptions.CopyToCliOrNot;
            //cbxFlashData.Checked=GeneralOptions.AutoFlashOrNot;
            //tmpTimeSpan = GeneralOptions.UpdateTimeSpan;
            //nudTimeSpan.Value = tmpTimeSpan;
            //checkCbxFlashData();     
            //cbxSaveSetting.Checked = GeneralOptions.SaveConfigOrNot;
            //cbxCatchClipboard.Checked = GeneralOptions.AutoCatchClipboardOrNot;
            //tmpKeyString = GeneralOptions.RemoveKeyString;            
            //tmpRemoveKeyStrChecked = GeneralOptions.RemoveStrOrNot;
            //checkCbxCatchClipboard();
            //checkCbxRemoveTxt();                   
        }
        //form加载事件
        private void FormSettings_Load(object sender, EventArgs e)
        {
            readSettings();
        }


        //剪切板功能
        private void cbxCatchClipboard_CheckedChanged(object sender, EventArgs e)
        {
            checkCbxCatchClipboard();
            checkCbxRemoveTxt();
        }

        private void checkCbxCatchClipboard()
        {
            if (cbxCatchClipboard.Checked == true)
            {
                cbxRemoveTxt.Enabled = true;               
            }
            else
            {
                cbxRemoveTxt.Enabled = false;
            }
        }
        //关键字开关
        private void cbxRemoveTxt_CheckedChanged(object sender, EventArgs e)
        {
            checkCbxRemoveTxt();
        }
        private void checkCbxRemoveTxt()
        {
            if (cbxRemoveTxt.Checked == true)
            {
                if (cbxCatchClipboard.Checked == true)
                {
                    txtRemovingStr.Enabled = true;
                }
                else
                {
                    txtRemovingStr.Enabled = false;
                }               
                txtRemovingStr.Text = tmpKeyString;
            }
            else
            {
                txtRemovingStr.Enabled = false;
            }
        }
        //关键字变化时复制
        private void txtRemovingStr_TextChanged(object sender, EventArgs e)
        {
            tmpKeyString = txtRemovingStr.Text.Trim();
        }
    }
}
