﻿namespace FlashNDCollection
{
    partial class FormTDCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTDCalculator));
            this.tlpTDcal = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtSliverLast = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtSliverOpen = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtGDLast = new System.Windows.Forms.TextBox();
            this.lblGDBtn = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblGDUD = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblGoldTD = new System.Windows.Forms.Label();
            this.lblSliverTD = new System.Windows.Forms.Label();
            this.lblSliverUD = new System.Windows.Forms.Label();
            this.lblSliverBtn = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtGDOpen = new System.Windows.Forms.TextBox();
            this.tlpTDcal.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpTDcal
            // 
            this.tlpTDcal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpTDcal.BackColor = System.Drawing.Color.White;
            this.tlpTDcal.ColumnCount = 5;
            this.tlpTDcal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpTDcal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpTDcal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpTDcal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpTDcal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpTDcal.Controls.Add(this.panel4, 2, 2);
            this.tlpTDcal.Controls.Add(this.panel3, 1, 2);
            this.tlpTDcal.Controls.Add(this.panel2, 2, 1);
            this.tlpTDcal.Controls.Add(this.lblGDBtn, 4, 1);
            this.tlpTDcal.Controls.Add(this.label5, 4, 0);
            this.tlpTDcal.Controls.Add(this.lblGDUD, 3, 1);
            this.tlpTDcal.Controls.Add(this.label1, 0, 0);
            this.tlpTDcal.Controls.Add(this.label2, 1, 0);
            this.tlpTDcal.Controls.Add(this.label3, 2, 0);
            this.tlpTDcal.Controls.Add(this.label4, 3, 0);
            this.tlpTDcal.Controls.Add(this.lblGoldTD, 0, 1);
            this.tlpTDcal.Controls.Add(this.lblSliverTD, 0, 2);
            this.tlpTDcal.Controls.Add(this.lblSliverUD, 3, 2);
            this.tlpTDcal.Controls.Add(this.lblSliverBtn, 4, 2);
            this.tlpTDcal.Controls.Add(this.panel1, 1, 1);
            this.tlpTDcal.Location = new System.Drawing.Point(0, 0);
            this.tlpTDcal.Margin = new System.Windows.Forms.Padding(2);
            this.tlpTDcal.Name = "tlpTDcal";
            this.tlpTDcal.RowCount = 3;
            this.tlpTDcal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpTDcal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpTDcal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tlpTDcal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpTDcal.Size = new System.Drawing.Size(370, 133);
            this.tlpTDcal.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.txtSliverLast);
            this.panel4.Location = new System.Drawing.Point(151, 91);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(68, 39);
            this.panel4.TabIndex = 24;
            // 
            // txtSliverLast
            // 
            this.txtSliverLast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSliverLast.BackColor = System.Drawing.Color.Silver;
            this.txtSliverLast.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSliverLast.Location = new System.Drawing.Point(3, 9);
            this.txtSliverLast.MaxLength = 6;
            this.txtSliverLast.Name = "txtSliverLast";
            this.txtSliverLast.Size = new System.Drawing.Size(58, 16);
            this.txtSliverLast.TabIndex = 1;
            this.txtSliverLast.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.txtSliverOpen);
            this.panel3.Location = new System.Drawing.Point(77, 91);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(68, 39);
            this.panel3.TabIndex = 23;
            // 
            // txtSliverOpen
            // 
            this.txtSliverOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSliverOpen.BackColor = System.Drawing.Color.Silver;
            this.txtSliverOpen.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSliverOpen.Location = new System.Drawing.Point(3, 9);
            this.txtSliverOpen.MaxLength = 6;
            this.txtSliverOpen.Name = "txtSliverOpen";
            this.txtSliverOpen.Size = new System.Drawing.Size(58, 16);
            this.txtSliverOpen.TabIndex = 1;
            this.txtSliverOpen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.Gold;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.txtGDLast);
            this.panel2.Location = new System.Drawing.Point(151, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(68, 38);
            this.panel2.TabIndex = 22;
            // 
            // txtGDLast
            // 
            this.txtGDLast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGDLast.BackColor = System.Drawing.Color.Gold;
            this.txtGDLast.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGDLast.Location = new System.Drawing.Point(3, 9);
            this.txtGDLast.MaxLength = 6;
            this.txtGDLast.Name = "txtGDLast";
            this.txtGDLast.Size = new System.Drawing.Size(58, 16);
            this.txtGDLast.TabIndex = 1;
            this.txtGDLast.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblGDBtn
            // 
            this.lblGDBtn.AllowDrop = true;
            this.lblGDBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGDBtn.BackColor = System.Drawing.Color.Gold;
            this.lblGDBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblGDBtn.Location = new System.Drawing.Point(299, 44);
            this.lblGDBtn.Name = "lblGDBtn";
            this.lblGDBtn.Size = new System.Drawing.Size(68, 44);
            this.lblGDBtn.TabIndex = 15;
            this.lblGDBtn.Text = "生成报价";
            this.lblGDBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblGDBtn.Click += new System.EventHandler(this.lblGDBtn_Click);
            // 
            // label5
            // 
            this.label5.AllowDrop = true;
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(299, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 44);
            this.label5.TabIndex = 14;
            this.label5.Text = "整合生成";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // lblGDUD
            // 
            this.lblGDUD.AllowDrop = true;
            this.lblGDUD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGDUD.BackColor = System.Drawing.Color.Gold;
            this.lblGDUD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblGDUD.Location = new System.Drawing.Point(225, 44);
            this.lblGDUD.Name = "lblGDUD";
            this.lblGDUD.Size = new System.Drawing.Size(68, 44);
            this.lblGDUD.TabIndex = 7;
            this.lblGDUD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AllowDrop = true;
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 44);
            this.label1.TabIndex = 0;
            this.label1.Text = "TD种类";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AllowDrop = true;
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Location = new System.Drawing.Point(77, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 44);
            this.label2.TabIndex = 1;
            this.label2.Text = "开盘";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AllowDrop = true;
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Location = new System.Drawing.Point(151, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 44);
            this.label3.TabIndex = 2;
            this.label3.Text = "昨收";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AllowDrop = true;
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Location = new System.Drawing.Point(225, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 44);
            this.label4.TabIndex = 3;
            this.label4.Text = "涨跌";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGoldTD
            // 
            this.lblGoldTD.AllowDrop = true;
            this.lblGoldTD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGoldTD.BackColor = System.Drawing.Color.Gold;
            this.lblGoldTD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblGoldTD.Location = new System.Drawing.Point(3, 44);
            this.lblGoldTD.Name = "lblGoldTD";
            this.lblGoldTD.Size = new System.Drawing.Size(68, 44);
            this.lblGoldTD.TabIndex = 5;
            this.lblGoldTD.Text = "黄金TD";
            this.lblGoldTD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSliverTD
            // 
            this.lblSliverTD.AllowDrop = true;
            this.lblSliverTD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSliverTD.BackColor = System.Drawing.Color.Silver;
            this.lblSliverTD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSliverTD.Location = new System.Drawing.Point(3, 88);
            this.lblSliverTD.Name = "lblSliverTD";
            this.lblSliverTD.Size = new System.Drawing.Size(68, 45);
            this.lblSliverTD.TabIndex = 6;
            this.lblSliverTD.Text = "白银TD";
            this.lblSliverTD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSliverUD
            // 
            this.lblSliverUD.AllowDrop = true;
            this.lblSliverUD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSliverUD.BackColor = System.Drawing.Color.Silver;
            this.lblSliverUD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSliverUD.Location = new System.Drawing.Point(225, 88);
            this.lblSliverUD.Name = "lblSliverUD";
            this.lblSliverUD.Size = new System.Drawing.Size(68, 45);
            this.lblSliverUD.TabIndex = 8;
            this.lblSliverUD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSliverBtn
            // 
            this.lblSliverBtn.AllowDrop = true;
            this.lblSliverBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSliverBtn.BackColor = System.Drawing.Color.Silver;
            this.lblSliverBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSliverBtn.Location = new System.Drawing.Point(299, 88);
            this.lblSliverBtn.Name = "lblSliverBtn";
            this.lblSliverBtn.Size = new System.Drawing.Size(68, 45);
            this.lblSliverBtn.TabIndex = 16;
            this.lblSliverBtn.Text = "生成报价";
            this.lblSliverBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSliverBtn.Click += new System.EventHandler(this.lblSliverBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Gold;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.txtGDOpen);
            this.panel1.Location = new System.Drawing.Point(77, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(68, 38);
            this.panel1.TabIndex = 21;
            // 
            // txtGDOpen
            // 
            this.txtGDOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGDOpen.BackColor = System.Drawing.Color.Gold;
            this.txtGDOpen.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGDOpen.Location = new System.Drawing.Point(3, 9);
            this.txtGDOpen.MaxLength = 6;
            this.txtGDOpen.Name = "txtGDOpen";
            this.txtGDOpen.Size = new System.Drawing.Size(58, 16);
            this.txtGDOpen.TabIndex = 0;
            this.txtGDOpen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FormTDCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(370, 133);
            this.Controls.Add(this.tlpTDcal);
            this.Font = new System.Drawing.Font("楷体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormTDCalculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TD开盘计算器";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormCalculator_Load);
            this.tlpTDcal.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpTDcal;
        private System.Windows.Forms.Label lblGDUD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblGoldTD;
        private System.Windows.Forms.Label lblSliverTD;
        private System.Windows.Forms.Label lblSliverUD;
        private System.Windows.Forms.Label lblSliverBtn;
        private System.Windows.Forms.Label lblGDBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtSliverLast;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtSliverOpen;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtGDLast;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtGDOpen;
    }
}