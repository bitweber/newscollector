﻿using System.Collections.Generic;
using HtmlAgilityPack;
using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace FlashNDCollection
{
    class WallStreetNewsCollection : ICatchFlashNews
    {
        public WallStreetNewsCollection()
        {
        }
        private string WallStreetUrl = @"http://api.wallstreetcn.com/v2/livenews?limit=60";
        private string WallStreetSiteName = "华尔街见闻";
        //public List<FlashNewsObject> getFlashNewsList()
        //{
        //    List<FlashNewsObject> newslist = new List<FlashNewsObject>();
        //    int newsid = 1;
        //    WebHtmlData webhtmldata = new WebHtmlData(WallStreetUrl);

        //    HtmlDocument wsdoc = new HtmlDocument();
        //    wsdoc.LoadHtml(webhtmldata.HtmlData);
        //    string shortdateStr = DateTime.Now.ToString("yyyy-MM-dd");

        //    HtmlNode Nodelist = wsdoc.GetElementbyId(@"livenews-list");
        //    HtmlNodeCollection NewsLevelNodes = Nodelist.SelectNodes(@"./ul/li");
        //    HtmlNodeCollection DataNodes = Nodelist.SelectNodes(@"./ul/li/div[@class='content']");
        //    HtmlNodeCollection TimeNodes = Nodelist.SelectNodes(@"./ul/li/a/span[@class='time']");
        //    for (int i = 0; i < DataNodes.Count; i++)
        //    {
        //        string tmpData = DataNodes[i].InnerText.Replace("\t", "").Trim();
        //        string tmpTime = FilterTools.trimAll(TimeNodes[i].InnerText);
        //        if (tmpTime.Length == 5)
        //        {
        //            tmpTime = tmpTime + ":00";
        //        }

        //        int newslevel = 1;
        //        if (tmpTime != "" && tmpData != "")
        //        {
        //            FlashNewsObject news = new FlashNewsObject();
        //            DataObject data = new DataObject();

        //            news.ID = newsid;
        //            //设置新闻重要性
        //            if (NewsLevelNodes[i].Attributes["data-importance"] != null)
        //            {
        //                string levelstr = NewsLevelNodes[i].Attributes["data-importance"].Value;
        //                newslevel = int.Parse(FilterTools.trimAll(levelstr));
        //            }

        //            //更新时间设置
        //            if (NewsLevelNodes[i].Attributes["class"] != null && NewsLevelNodes[i].Attributes["class"].Value == "date")
        //            {
        //                string dateFinder = FilterTools.trimAll(NewsLevelNodes[i].InnerText);
        //                dateFinder = dateFinder.Replace("年", "-").Replace("月", "-").Replace("日", "");
        //                shortdateStr = dateFinder;
        //            }

        //            news.Level = newslevel;

        //            news.Time = DateTime.ParseExact(string.Format("{0} {1}", shortdateStr, FilterTools.trimAll(tmpTime)), "yyyy-MM-dd HH:mm:ss", null);
        //            news.News = tmpData;
        //            news.IsUsed = false;
        //            news.FromSite = WallStreetSiteName;
        //            //过滤新闻
        //            if (FilterTools.Level != 0)
        //            {
        //                if (news.Level == FilterTools.Level)
        //                {
        //                    newslist.Add(news);
        //                    newsid++;
        //                }
        //            }
        //            else
        //            {
        //                newslist.Add(news);
        //                newsid++;
        //            }
        //        }
        //    }
        //    return newslist;
        //}

        //public List<FlashNewsObject> getFlashNewsList()
        //{
        //    List<FlashNewsObject> newslist = new List<FlashNewsObject>();
        //    int newsid = 1;
        //    WebHtmlData webhtmldata = new WebHtmlData(WallStreetUrl);

        //    FlashNewsObject news = new FlashNewsObject();
        //    DataObject data = new DataObject();

        //    HtmlDocument wsdoc = new HtmlDocument();
        //    wsdoc.LoadHtml(webhtmldata.HtmlData);
        //    string shortdateStr = DateTime.Now.ToString("yyyy-MM-dd");

        //    HtmlNode Nodelist = wsdoc.GetElementbyId("global");
        //    HtmlNodeCollection NewsNodes = Nodelist.SelectNodes("./div[@class='livenews-list']/ul/li");

        //    for (int i = 0; i < NewsNodes.Count; i++)
        //    {
        //        string tmpTime = "";
        //        string tmpData = "";

        //        //更新时间设置
        //        if (NewsNodes[i].Attributes["class"] != null && NewsNodes[i].Attributes["class"].Value == "date")
        //        {
        //            string dateFinder = FilterTools.trimAll(NewsNodes[i].InnerText);
        //            //dateFinder = dateFinder.Replace("年", "-").Replace("月", "-").Replace("日", "");
        //            dateFinder = dateFinder.Replace(".", "-");
        //            shortdateStr = dateFinder;
        //        }

        //        if (NewsNodes[i].SelectSingleNode("./a/span") != null)
        //        {
        //            tmpTime = FilterTools.trimAll(NewsNodes[i].SelectSingleNode("./a/span").InnerText);
        //            if (tmpTime.Length == 5)
        //            {
        //                tmpTime = tmpTime + ":00";
        //            }
        //        }

        //        if (NewsNodes[i].SelectSingleNode("./div/div[1]/div[1]") != null)
        //        {
        //            tmpData = FilterTools.trimAll(NewsNodes[i].SelectSingleNode(@"./div/div[1]/div[1]").InnerText);
        //        }

        //        int newslevel = 1;
        //        if (tmpTime != "" && tmpData != "")
        //        {
        //            news = new FlashNewsObject();
        //            data = new DataObject();

        //            news.ID = newsid;
        //            //设置新闻重要性
        //            if (NewsNodes[i].Attributes["data-importance"] != null)
        //            {
        //                string levelstr = NewsNodes[i].Attributes["data-importance"].Value;
        //                newslevel = int.Parse(FilterTools.trimAll(levelstr));
        //            }

        //            news.Level = newslevel;

        //            news.Time = DateTime.ParseExact(string.Format("{0} {1}", shortdateStr, FilterTools.trimAll(tmpTime)), "yyyy-MM-dd HH:mm:ss", null);
        //            news.News = tmpData;
        //            news.IsUsed = false;
        //            news.FromSite = WallStreetSiteName;
        //            //过滤新闻
        //            if (FilterTools.Level != 0)
        //            {
        //                if (news.Level == FilterTools.Level)
        //                {
        //                    newslist.Add(news);
        //                    newsid++;
        //                }
        //            }
        //            else
        //            {
        //                newslist.Add(news);
        //                newsid++;
        //            }
        //        }
        //    }
        //    return newslist;
        //}

        public List<FlashNewsObject> getFlashNewsList()
        {
            List<FlashNewsObject> newslist = new List<FlashNewsObject>();
            int newsid = 1;
            WebHtmlData webhtmldata = new WebHtmlData(WallStreetUrl);

            FlashNewsObject news = new FlashNewsObject();
            string newsjson = webhtmldata.HtmlData;

            Dictionary<string, object> dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(newsjson);

            //object obj = dic["results"];

            string jsondata = dic["results"].ToString();

            JArray jarr = (JArray)JsonConvert.DeserializeObject(jsondata);

            for (int i = 0; i < jarr.Count; i++)
            {
                news = new FlashNewsObject();
                Dictionary<string, object> newsdic = JsonConvert.DeserializeObject<Dictionary<string, object>>(jarr[i].ToString());

                news.ID = newsid;
                news.Time = FilterTools.StampToDateTime(newsdic["createdAt"].ToString());

                news.News = newsdic["contentText"].ToString();

                if (newsdic["node_color"].ToString().Contains("红色"))
                {
                    news.Level = 3;
                }
                else
                {
                    news.Level = 1;
                }
                news.FromSite = WallStreetSiteName;
                newslist.Add(news);
                newsid++;
            }
            return newslist;
        }

    }
}
