﻿using System;
namespace FlashNDCollection
{
    class FlashNewsObject
    {
        public string FromSite { get; set; } //来源网站
        public int ID { get; set; }//新闻ID
        public int Level { get; set; }//新闻重要级别
        public DateTime Time { get; set; }//更新时间
        public string News { get; set; }//新闻
        public bool IsUsed { get; set; }//是否更新过
        public FlashNewsObject() { }
    }
}
