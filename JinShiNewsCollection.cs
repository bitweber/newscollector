﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FlashNDCollection
{
    class JinShiNewsCollection : ICatchData,ICatchFlashNews
    {
        public JinShiNewsCollection()
        {

        }
      
        private string JinShiUrl = @"http://m.jin10.com/flash?jsonpCallback=jQuery&maxId=0&count=100&_=";
        private string JinShiSiteName = "金十数据";
        public List<DataObject> getDataList()
        {
            List<DataObject> DTlist = new List<DataObject>();
            int dataid = 1;

            DataObject DT = new DataObject();

            WebHtmlData webhtmldata = new WebHtmlData(JinShiUrl);

            string jsstr = webhtmldata.HtmlData.Replace(@"/**/jQuery(", "").TrimEnd(';').TrimEnd(')');

            JArray jsonArr = (JArray)JsonConvert.DeserializeObject(jsstr);

            for (int i = 0; i < jsonArr.Count; i++)
            {
                DT = new DataObject();
                string[] item = jsonArr[i].ToString().Split('#');
                if (item[0] == "1")
                {
                    DT.ID = dataid;
                    DT.Level = Int32.Parse(item[6]);
                    DT.Time = DateTime.ParseExact(item[8], "yyyy-MM-dd HH:mm:ss", null);
                    DT.DataName = item[2];
                    DT.PreValue = item[3];
                    DT.Expectedvalue = item[4];
                    DT.ActualValue = item[5];
                    DT.DataAffacts = item[7];
                    if (DT.DataAffacts.Contains("利多"))
                    {
                        DT.DataAffacts = "黄金或受支撑";
                    }
                    else if (DT.DataAffacts.Contains("利空"))
                    {
                        DT.DataAffacts = "黄金或将承压";
                    }
                    else
                    {
                        DT.DataAffacts = "";
                    }

                    if (DT.PreValue.Contains("%") && DT.ActualValue.Contains("%"))
                    {
                        DT.PercentTypeOrNot = true;
                    }
                    else
                    {
                        DT.PercentTypeOrNot = false;
                    }
                    //预期比较
                    if (DT.PercentTypeOrNot == true)
                    {
                        double dataAc = double.Parse(DT.ActualValue.Replace("%", ""));
                        double dataExp = double.Parse(DT.Expectedvalue.Replace("%", ""));
                        if (dataAc > dataExp)
                        {
                            //if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
                            //{
                            //    data.AcPreCompare = "不及预期";
                            //}
                            //else
                            //{
                            //    data.AcPreCompare = "好于预期";
                            //}
                            DT.AcPreCompare = "好于预期";
                        }
                        else if (dataAc == dataExp)
                        {
                            DT.AcPreCompare = "符合预期";
                        }
                        else
                        {
                            //if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
                            //{
                            //    data.AcPreCompare = "好于预期";
                            //}
                            //else
                            //{
                            //    data.AcPreCompare = "不及预期";
                            //}
                            DT.AcPreCompare = "不及预期";
                        }
                    }
                    else
                    {
                        double dataAc = double.Parse(DT.ActualValue);
                        double dataExp = double.Parse(DT.Expectedvalue);
                        if (dataAc > dataExp)
                        {
                            //if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
                            //{
                            //    data.AcPreCompare = "不及预期";
                            //}
                            //else
                            //{
                            //    data.AcPreCompare = "好于预期";
                            //}
                            DT.AcPreCompare = "好于预期";
                        }
                        else if (dataAc == dataExp)
                        {
                            DT.AcPreCompare = "符合预期";
                        }
                        else
                        {
                            //if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
                            //{
                            //    data.AcPreCompare = "好于预期";
                            //}
                            //else
                            //{
                            //    data.AcPreCompare = "不及预期";
                            //}
                            DT.AcPreCompare = "不及预期";
                        }
                    }
                    DT.FromSite = JinShiSiteName;
                    //过滤数据
                    if (FilterTools.Level != 0)
                    {
                        if (DT.Level == FilterTools.Level)
                        {
                            DTlist.Add(DT);
                            dataid++;
                        }
                    }
                    else
                    {
                        DTlist.Add(DT);
                        dataid++;
                    }
                }
            }       
            return DTlist;
        }

        //public List<DataObject> getDataList()
        //{
        //    List<DataObject> datalist = new List<DataObject>();
        //    int dataid = 1;

        //    WebHtmlData webhtmldata = new WebHtmlData(JinShiUrl);
        //    HtmlDocument doc = new HtmlDocument();
        //    doc.LoadHtml(webhtmldata.HtmlData);

        //    HtmlNode Nodelist = doc.GetElementbyId("newslist");

        //    HtmlNodeCollection DivCounts = Nodelist.SelectNodes(@"./div");
        //    HtmlNodeCollection LevelNodes = Nodelist.SelectNodes(@"./div[@class='newsline']/table");
        //    HtmlNodeCollection NewsDataNodes = Nodelist.SelectNodes(@"./div[@class='newsline']/table/tr/td[3]");
        //    HtmlNodeCollection NewsDataUpdateTimeNodes = Nodelist.SelectNodes(@"./div[@class='newsline']/table/tr/td[2]");

        //    string shortdateStr = DateTime.Now.ToString("yyyy-MM-dd");

        //    for (int i = 0; i < NewsDataNodes.Count; i++)
        //    {
        //        int datalevel = 1;
        //        寻找更新日期

        //        if (DivCounts[i].Attributes["id"].Value == "navs_0")
        //        {
        //            string dateFinder = FilterTools.trimAll(DivCounts[i].InnerText);
        //            dateFinder = FilterTools.catchStr(dateFinder, "以下为", "日").Replace("年", "-").Replace("月", "-");
        //            shortdateStr = dateFinder;
        //        }
        //        string tmpTime = "";
        //        if (NewsDataUpdateTimeNodes[i] != null)
        //        {
        //            tmpTime = FilterTools.trimAll(NewsDataUpdateTimeNodes[i].InnerText);
        //            if (tmpTime.Length == 5)
        //            {
        //                tmpTime = tmpTime + ":00";
        //            }
        //            tmpTime = string.Format("{0} {1}", shortdateStr, tmpTime);
        //        }

        //        HtmlNode Nodes_dname = NewsDataNodes[i].SelectSingleNode(@"./table/tr[1]/td[3]");//获取数据名称
        //        HtmlNode Nodes_dactualvalue = NewsDataNodes[i].SelectSingleNode(@"./table/tr/td[5]");//获取实际值
        //        HtmlNode Nodes_dpre_exp_value = NewsDataNodes[i].SelectSingleNode(@"./table/tr[2]/td[1]");//前值和预期值获取
        //        HtmlNode Nodes_dcorrected_pre_exp_value = NewsDataNodes[i].SelectSingleNode(@"./table/tr[2]/td[1]/div[1]");//修正值获取
        //        HtmlNode Nodes_daffects = NewsDataNodes[i].SelectSingleNode(@"./table/tr/td[6]");//获取数据影响
        //        HtmlNode Nodes_dlevels = NewsDataNodes[i].SelectSingleNode(@"./table/tr/td[4]/img");//获取数据的级别  

        //        if (Nodes_dname != null && Nodes_dactualvalue != null && Nodes_daffects != null && Nodes_dlevels != null && tmpTime != null && Nodes_dpre_exp_value != null)
        //        {

        //            DataObject data = new DataObject();

        //            data.ID = dataid;
        //            datalevel = int.Parse(FilterTools.trimAll(Nodes_dlevels.Attributes["title"].Value).Replace("星数据", ""));
        //            data.Level = datalevel;
        //            data.DataName = Nodes_dname.InnerText.Replace("\r\n", "").Trim();
        //            data.PercentTypeOrNot = false;
        //            string actualvalue = FilterTools.trimAll(Nodes_dactualvalue.InnerText).Replace("实际：", "");
        //            data.ActualValue = actualvalue;
        //            if (actualvalue.Contains("%"))
        //            {
        //                data.PercentTypeOrNot = true;
        //            }

        //            string pre_exp_value = FilterTools.trimAll(Nodes_dpre_exp_value.InnerText);//预期值和前值的获取
        //            string corrected_pre_value = FilterTools.trimAll(Nodes_dcorrected_pre_exp_value.InnerText);//修正值

        //            if (corrected_pre_value != "")
        //            {
        //                data.PreValue = FilterTools.catchStr(corrected_pre_value, "修正：", "（前值）").Trim();
        //                data.Expectedvalue = FilterTools.catchStr(pre_exp_value, "预期：", "修正").Trim();
        //            }
        //            else
        //            {
        //                data.PreValue = FilterTools.catchStr(pre_exp_value, "前值：", "预期").Trim();
        //                data.Expectedvalue = FilterTools.catchStr(pre_exp_value, "预期：", "").Trim();
        //            }
        //            预期比较
        //            if (data.PercentTypeOrNot == true)
        //            {
        //                double dataAc = double.Parse(data.ActualValue.Replace("%", ""));
        //                double dataExp = double.Parse(data.Expectedvalue.Replace("%", ""));
        //                if (dataAc > dataExp)
        //                {
        //                    if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
        //                    {
        //                        data.AcPreCompare = "不及预期";
        //                    }
        //                    else
        //                    {
        //                        data.AcPreCompare = "好于预期";
        //                    }
        //                }
        //                else if (dataAc == dataExp)
        //                {
        //                    data.AcPreCompare = "符合预期";
        //                }
        //                else
        //                {
        //                    if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
        //                    {
        //                        data.AcPreCompare = "好于预期";
        //                    }
        //                    else
        //                    {
        //                        data.AcPreCompare = "不及预期";
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                double dataAc = double.Parse(data.ActualValue);
        //                double dataExp = double.Parse(data.Expectedvalue);
        //                if (dataAc > dataExp)
        //                {
        //                    if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
        //                    {
        //                        data.AcPreCompare = "不及预期";
        //                    }
        //                    else
        //                    {
        //                        data.AcPreCompare = "好于预期";
        //                    }
        //                }
        //                else if (dataAc == dataExp)
        //                {
        //                    data.AcPreCompare = "符合预期";
        //                }
        //                else
        //                {
        //                    if (data.DataName.Contains("API") || data.DataName.Contains("EIA") || data.DataName.Contains("失业") || data.DataName.Contains("裁员"))
        //                    {
        //                        data.AcPreCompare = "好于预期";
        //                    }
        //                    else
        //                    {
        //                        data.AcPreCompare = "不及预期";
        //                    }
        //                }
        //            }

        //            data.Time = DateTime.ParseExact(tmpTime, "yyyy-MM-dd HH:mm:ss", null);
        //            data.IsUsed = false;
        //            data.DataAffacts = FilterTools.trimAll(Nodes_daffects.InnerText).Replace("无影响金银", "影响较小");
        //            if (data.DataAffacts == "利多金银")
        //            {
        //                data.DataAffacts = "黄金或将受支撑";
        //            }
        //            else if (data.DataAffacts == "利空金银")
        //            {
        //                data.DataAffacts = "黄金或将承压";
        //            }
        //            data.FromSite = JinShiSiteName;
        //            过滤数据
        //            if (FilterTools.Level != 0)
        //            {
        //                if (data.Level == FilterTools.Level)
        //                {
        //                    datalist.Add(data);
        //                    dataid++;
        //                }
        //            }
        //            else
        //            {
        //                datalist.Add(data);
        //                dataid++;
        //            }
        //        }
        //    }

        //    return datalist;
        //}



        public List<FlashNewsObject> getFlashNewsList()
        {
            List<FlashNewsObject> newslist = new List<FlashNewsObject>();
            int newsid = 1;
            WebHtmlData webhtmldata = new WebHtmlData(JinShiUrl);

            FlashNewsObject news = new FlashNewsObject();

            string jsstr = webhtmldata.HtmlData.Replace(@"/**/jQuery(", "").TrimEnd(';').TrimEnd(')');

            JArray jsonArr = (JArray)JsonConvert.DeserializeObject(jsstr);

            for (int i = 0; i < jsonArr.Count; i++)
            {
                news = new FlashNewsObject();
                string[] item = jsonArr[i].ToString().Split('#');
                if (item[0] == "0" && !item[3].Contains("href"))
                {
                    news.Time = DateTime.ParseExact(item[2], "yyyy-MM-dd HH:mm:ss", null);
                    news.ID = newsid;
                    news.Level = Int32.Parse(item[1]);
                    if (news.Level==0)
                    {
                        news.Level = 3;
                    }
                    news.News = item[3].Replace(@"<br />", "\r\n").Replace("<b>", "").Replace("</b>", "").Replace("</font>", "").Replace("</br >", "\r\n").Replace("</br>", "\r\n");
                    if (news.News.Contains("important-text"))
                    {
                        news.News = news.News.Remove(0, 35);
                        news.Level = 2;
                    }
                    news.FromSite = JinShiSiteName;
                    //过滤新闻
                    if (FilterTools.Level != 0)
                    {
                        if (news.Level == FilterTools.Level)
                        {
                            newslist.Add(news);
                            newsid++;
                        }
                    }
                    else
                    {
                        newslist.Add(news);
                        newsid++;
                    }
                }
            }
            return newslist;
        }


        //public List<FlashNewsObject> getFlashNewsList()
        //{
        //    List<FlashNewsObject> newslist = new List<FlashNewsObject>();
        //    int newsid = 1;
        //    string shortdateStr = DateTime.Now.ToString("yyyy-MM-dd");

        //    WebHtmlData webhtmldata = new WebHtmlData(JinShiUrl);
        //    HtmlDocument doc = new HtmlDocument();
        //    doc.LoadHtml(webhtmldata.HtmlData);

        //    string nowDate = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();

        //    string id = "J_flash_wrap" + nowDate;

        //    HtmlNode Nodelist = doc.GetElementbyId("J_list");

        //    //HtmlNode Nodelist = Nodelist_1.SelectSingleNode(@"./div[2]");

        //    HtmlNodeCollection NewsDataNodes = Nodelist.SelectNodes(@"./li");

        //    for (int i = 0; i < NewsDataNodes.Count; i++)
        //    {

        //        int newslevel = 1;

        //        string datatimestr = NewsDataNodes[i].Attributes["id"].Value.Substring(0,7);

        //        //if (datatimeNode != null && datatimeNode.InnerText != null & datatimeNode.InnerText != "")
        //        //{
        //        //    string dateFinder = FilterTools.trimAll(datatimeNode.InnerText);
        //        //    dateFinder = dateFinder.Replace("年", "-").Replace("月", "-").Replace("日", "");
        //        //    dateFinder = FilterTools.regexData(dateFinder, @"(?<=).*(?=星)");
        //        //    if (dateFinder != "")
        //        //    {
        //        //        shortdateStr = dateFinder;
        //        //    }
        //        //}

        //        HtmlNode timeNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='oem-content_h']/span[@class='oem-content_date']");
        //        HtmlNode newsNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='oem-content_b']/p[@class='oem-content_text']");

        //        //HtmlNode dataNode = NewsDataNodes[i].SelectSingleNode(@"./div[@class='jin-flash_b']/div[@class='jin-flash_data']");

        //        //寻找更新日期

        //        //if (NewsDataNodes[i].Attributes["id"].Value == "navs_0")
        //        //{
        //        //    string dateFinder = FilterTools.trimAll(NewsDataNodes[i].InnerText);
        //        //    dateFinder = FilterTools.catchStr(dateFinder, "以下为", "日").Replace("年", "-").Replace("月", "-");
        //        //    shortdateStr = dateFinder;
        //        //}

        //        string tmpNewsOrData = "";
        //        string tmpTime = "";
        //        if (timeNode != null)
        //        {
        //            tmpTime = FilterTools.trimAll(timeNode.InnerText);
        //            if (tmpTime.Length == 5)
        //            {
        //                tmpTime = tmpTime + ":00";
        //            }
        //            tmpTime = string.Format("{0} {1}", shortdateStr, tmpTime);
        //        }

        //        //if (dataNode == null && newsNode != null)
        //        //{
        //        //    tmpNewsOrData = newsNode.InnerText.Replace("\t", "").Trim();
        //        //}


        //        if (tmpTime != "" && tmpNewsOrData != "")
        //        {
        //            FlashNewsObject news = new FlashNewsObject();
        //            news.ID = newsid;
        //            //寻找newslevel
        //            if (NewsDataNodes[i].Attributes["class"] != null)
        //            {
        //                if (NewsDataNodes[i].Attributes["class"].Value.Contains("important"))
        //                {
        //                    newslevel = 3;
        //                }                      
        //            }

        //            if (tmpNewsOrData.Contains("美联储") || tmpNewsOrData.Contains("黄金") || tmpNewsOrData.Contains("金价") || tmpNewsOrData.Contains("央行"))
        //            {
        //                newslevel = 3;
        //            }

        //            news.Level = newslevel;
        //            news.Time = DateTime.ParseExact(tmpTime, "yyyy-MM-dd HH:mm:ss", null);
        //            news.News = tmpNewsOrData;
        //            news.FromSite = JinShiSiteName;
        //            //过滤新闻
        //            if (FilterTools.Level != 0)
        //            {
        //                if (news.Level == FilterTools.Level)
        //                {
        //                    newslist.Add(news);
        //                    newsid++;
        //                }
        //            }
        //            else
        //            {
        //                newslist.Add(news);
        //                newsid++;
        //            }
        //        }
        //    }
        //    return newslist;
        //}


    }
}
