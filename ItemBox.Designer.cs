﻿namespace FlashNDCollection
{
    partial class ItemBox
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutShow = new System.Windows.Forms.TableLayoutPanel();
            this.lblTime = new System.Windows.Forms.Label();
            this.txtItemBox = new System.Windows.Forms.TextBox();
            this.cbxSelect = new System.Windows.Forms.CheckBox();
            this.lblLevelStar = new System.Windows.Forms.Label();
            this.tableLayoutShow.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutShow
            // 
            this.tableLayoutShow.BackColor = System.Drawing.Color.White;
            this.tableLayoutShow.ColumnCount = 4;
            this.tableLayoutShow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutShow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3F));
            this.tableLayoutShow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77F));
            this.tableLayoutShow.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutShow.Controls.Add(this.lblTime, 0, 0);
            this.tableLayoutShow.Controls.Add(this.txtItemBox, 2, 0);
            this.tableLayoutShow.Controls.Add(this.cbxSelect, 3, 0);
            this.tableLayoutShow.Controls.Add(this.lblLevelStar, 1, 0);
            this.tableLayoutShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutShow.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutShow.Name = "tableLayoutShow";
            this.tableLayoutShow.RowCount = 1;
            this.tableLayoutShow.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutShow.Size = new System.Drawing.Size(776, 60);
            this.tableLayoutShow.TabIndex = 0;
            // 
            // lblTime
            // 
            this.lblTime.BackColor = System.Drawing.Color.AliceBlue;
            this.lblTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTime.Font = new System.Drawing.Font("楷体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTime.ForeColor = System.Drawing.Color.Black;
            this.lblTime.Location = new System.Drawing.Point(3, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(110, 60);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "时间显示";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTime.Click += new System.EventHandler(this.lblTime_Click);
            // 
            // txtItemBox
            // 
            this.txtItemBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtItemBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemBox.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtItemBox.Location = new System.Drawing.Point(142, 3);
            this.txtItemBox.Multiline = true;
            this.txtItemBox.Name = "txtItemBox";
            this.txtItemBox.Size = new System.Drawing.Size(591, 54);
            this.txtItemBox.TabIndex = 1;
            this.txtItemBox.Text = "内容显示区域";
            // 
            // cbxSelect
            // 
            this.cbxSelect.BackColor = System.Drawing.Color.AliceBlue;
            this.cbxSelect.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbxSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbxSelect.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbxSelect.ForeColor = System.Drawing.Color.Red;
            this.cbxSelect.Location = new System.Drawing.Point(739, 3);
            this.cbxSelect.Name = "cbxSelect";
            this.cbxSelect.Size = new System.Drawing.Size(34, 54);
            this.cbxSelect.TabIndex = 2;
            this.cbxSelect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbxSelect.UseVisualStyleBackColor = false;
            this.cbxSelect.CheckedChanged += new System.EventHandler(this.cbxSelect_CheckedChanged);
            // 
            // lblLevelStar
            // 
            this.lblLevelStar.BackColor = System.Drawing.Color.AliceBlue;
            this.lblLevelStar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLevelStar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblLevelStar.Font = new System.Drawing.Font("楷体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblLevelStar.ForeColor = System.Drawing.Color.Red;
            this.lblLevelStar.Location = new System.Drawing.Point(119, 0);
            this.lblLevelStar.Name = "lblLevelStar";
            this.lblLevelStar.Size = new System.Drawing.Size(17, 60);
            this.lblLevelStar.TabIndex = 3;
            this.lblLevelStar.Text = "★★★★★";
            this.lblLevelStar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ItemBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutShow);
            this.Font = new System.Drawing.Font("楷体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ItemBox";
            this.Size = new System.Drawing.Size(776, 60);
            this.Load += new System.EventHandler(this.SingleItemShow_Load);
            this.tableLayoutShow.ResumeLayout(false);
            this.tableLayoutShow.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutShow;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.TextBox txtItemBox;
        private System.Windows.Forms.CheckBox cbxSelect;
        private System.Windows.Forms.Label lblLevelStar;
    }
}
