﻿using System;

namespace FlashNDCollection
{
    class DataObject
    {
        public string FromSite { get; set; }//来源网站
        public int ID { get; set; }//数据ID
        public int Level { get; set; }//数据级别
        public  DateTime Time { get; set; }//数据更新时间
        public string DataName { get; set; }//数据名称
        public string DataAffacts { get; set; }//数据影响
        public string ActualValue { get; set; }//实际值
        public string PreValue { get; set; }//前值        
        public string Expectedvalue { get; set; } //预期值
        public bool IsUsed { get; set; }//是否更新过

        public string AcPreCompare;//与预期值相比较
        public bool PercentTypeOrNot;
        public DataObject() { }
    }
}
