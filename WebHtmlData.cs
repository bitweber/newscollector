﻿using System.Text;
using System.Net;
using System.IO;
using System;

namespace FlashNDCollection
{
    class WebHtmlData
    {
        public string HtmlData { get; }

        public WebHtmlData(string url)
        {
            HtmlData = getHtmlData(url);
        }

        private string getHtmlData(string url)
        {
            try
            {
                string htmlData = "";
                WebRequest.DefaultWebProxy = null;              
                HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(url);
                rq.Accept = "*/*";
                rq.UserAgent = "Chrome Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";
                rq.Proxy = null;
                rq.Connection = "KeepAlive";
                rq.KeepAlive = true;
                rq.Method = "GET";
                rq.Timeout = 4000;
                HttpWebResponse rp = (HttpWebResponse)rq.GetResponse();
                Stream resStream = rp.GetResponseStream();
                Encoding enc = Encoding.GetEncoding("utf-8"); // 如果是乱码就改成 utf-8 / GB2312
                using (StreamReader sr = new StreamReader(resStream, enc))  //命名空间:System.IO。
                {
                    htmlData = sr.ReadToEnd();
                }
                return htmlData;
            }
            catch
            {
                throw new Exception("404");
            }
        }
    }
}
